from setuptools import setup

setup(name='Newton',
      version='1.0',
      description='OpenShift App',
      author='Mohan Ponnada',
      author_email='kuldeep@unixell.in',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask>=0.7.2','flask-login','requests','psycopg2','flask-wtf','boto','sqlalchemy','m2crypto','twilio','flask_oauth']
     )
