from myflaskapp import 


CREATE SEQUENCE seq_property_id;

CREATE TABLE newton_property (
property_id int default(nextval('seq_property_id')),
property_nm varchar(64),
property_desc varchar(255),
property_building_progress varchar(3),
property_funding_progress varchar(3),
builder_id int not null,
create_dttm date,
start_dttm date,
complete_dttm date,
funding_start_dttm date,
funding_complete_dttm date,
returns varchar(10),
fee  varchar(3),
approval_status varchar(3),
property_url_snapshot varchar(255),
primary key(property_id)
);

CREATE SEQUENCE seq_user_id;


CREATE TABLE newton_user (
   user_id		int default(nextval('seq_user_id')),
   last_nm		varchar(64) not null,
   first_nm		varchar(64) not null,
   middle_nm		varchar(64) not null ,
   primary_no		varchar(64),
   email		varchar(100),   
   facebook_uname	varchar(100),
   linkedin_uname	varchar(100),
   create_dttm 		date,
   update_dttm 		date,
   modhash		varchar(64),
   active		boolean,
   password		varchar(128),
   salt			varchar(128),
   credit		varchar(10),
   address		varchar(255),
   user_flag	varchar(10),
   registered_name varchar(64),
   registration_number varchar(64),
   primary key(user_id)
   
);


CREATE SEQUENCE seq_asset_id;

CREATE TABLE newton_asset(
	asset_id  int default(nextval('seq_asset_id')),
	asset_url varchar(255),
	asset_desc varchar(255),
	asset_type varchar(2) ,/*comment 'V(video) I(Image) D(Document)'*/
	property_id int not null,
	primary key(asset_id)
);

CREATE SEQUENCE seq_banker_id;

CREATE TABLE newton_banker(
	banker_id  int default(nextval('seq_banker_id')),
	banker_name varchar(100),
	banker_logo_url varchar(255),
	banker_desc varchar(255),
	primary key(banker_id)
);

CREATE TABLE newton_builder_property(
	builder_id int not null,
	property_id int not null,
	primary key(builder_id,property_id)
);

CREATE TABLE newton_user_property(
	user_id int not null,
	property_id int not null,
	invested_value varchar(15),
	invested_dttm date,
	roi varchar(15),	
	primary key(user_id,property_id)
);

CREATE TABLE newton_banker_property(
	property_id int not null,
	banker_id int not null,
	banker_investment varchar(15),
	banker_roi varchar(15),
	primary key(property_id,banker_id)
);
	

CREATE TABLE newton_user_payment(	
	user_id int not null,
	payment_id int,
	payment_value varchar(15),
	payment_acct_name varchar(255),
	payment_acct_no varchar(64),
	payment_acct_bank varchar(255),
	payment_acct_branch_address varchar(255),
	payment_dttm date,
	transaction_id varchar(100),
	proof_url varchar(255),
	primary key(transaction_id)
);


CREATE SEQUENCE userSeq;

CREATE TABLE userdata (
    user_i_id       integer  default nextval('userSeq'),
    user_id	      varchar(15),    
    user_device     varchar(11),
    user_age	       varchar(20),
    user_gender     varchar(1),
    user_income     varchar(1),
    user_occupation varchar(20),
    user_race       varchar(2),
    user_date       timestamp,
    primary key(user_i_id)
);


CREATE SEQUENCE segmentsSeq;

CREATE TABLE segments (
    segment_id        integer  default nextval('segmentsSeq'),
    segment_name      varchar(50),
    segment_age       varchar(20),
    segment_occupation varchar(255),
    segment_gender     varchar(1),
    segment_income     varchar(2),
    segment_race       varchar(3),
    primary key(segment_id)
);



CREATE SEQUENCE trafficSeq;

CREATE TABLE usertraffic (
    traffic_id	      integer  default nextval('trafficSeq'),
    user_i_id         integer,
    user_id	          varchar(15),    
    visit_at	      timestamp default NULL,
    primary key(trafficSeq_id)    
);


CREATE SEQUENCE campaignSeq;

CREATE TABLE campaign(
  campaign_id  integer default nextval('campaignSeq'),
  campaign_name  VARCHAR(50),
  campaign_assertid integer ,
  primary key(campaign_id)
); 


CREATE SEQUENCE assertseq;
CREATE TABLE campaign_asserts(
  asserts_id integer default nextval('assertseq'),
  assert_type varchar(255),
  asserts_clicks Integer,
  asserts_views  Integer,
  asserts_ratio  Integer,
  camp_id Integer,
  primary key(asserts_id),
  FOREIGN KEY (camp_id ) 
  REFERENCES campaign(campaign_id) 
);

CREATE SEQUENCE campaignsseq;

CREATE TABLE campaigns
(
  campaign_id integer default nextval('campaignsseq'),
  campaign_name character varying(255),
  start_date date,
  end_date date,
  budget bigint,
  frequency_cap bigint,
  gender character varying(20),
  income character varying(20),
  agegroup character varying(20),
  occupation character varying(20),
  race character varying(20),
  recency character varying(30),
  frequency character varying(30),
  monthly_income character varying(30),
  target_visitor character varying(30),
  distance_range character varying(30),
  mobile_sms character varying(255),
  banner character varying(255),
  video character varying(255),
  primary key(campaign_id)
);

CREATE SEQUENCE detailsseq;
CREATE TABLE details
( 
  details_id integer default nextval('detailsseq'),
  details_date date,
  details_time timestamp,
  details_content character varying(30),
  details_count integer,
  details_duration timestamp,
  details_avgpixels  numeric(10,2),
  details_rate integer,
  details_price character varying(20),
  primary key(details_id)
);

CREATE SEQUENCE brandseq;
CREATE TABLE brands 
( 
  brand_id integer default nextval('brandseq'),
  brand_name character varying(20),
  primary key(brand_id)
);

CREATE SEQUENCE stationseq;
CREATE TABLE stations
(
station_id integer default nextval('staionseq'),
station_name character varying(20),
primary key(station_id)
);


CREATE SEQUENCE seqstoreid;
CREATE TABLE store(
    storekey_id  integer default nextval('seqstoreid'),
    store_id varchar(20),
    store_date date,
    store_hour integer,
    store_min integer,
    store_gender varchar(1),
    store_age  varchar(20),
    primary key(storekey_id)
    );

CREATE SEQUENCE seqproofid;
CREATE TABLE proof(
		proof_id integer default nextval('seqproofid'),
		proof_date date,
		proof_hour integer,
		proof_min varchar(3),
		proof_package varchar(20),
		proof_station varchar(20),
		proof_content varchar(20),
		proof_count integer,
		proof_duration varchar(10),
		proof_AvgMpixel varchar(10),
		proof_rate integer,
		proof_price integer,
		primary key(proof_id)
		);

ALTER SEQUENCE seq_banker_id RESTART WITH 101;
