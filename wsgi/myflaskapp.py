from flask import Flask, request, flash, url_for, redirect, \
     render_template, abort, session,Response, json,jsonify,make_response,current_app
from datetime import timedelta
import datetime
import json
from functools import update_wrapper
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func,distinct
from models import *
from sqlalchemy import and_
import urllib2
from sqlalchemy import *
import math
from flask import Flask, request, flash, url_for, redirect, \
     render_template, abort, session, Response, json,jsonify,make_response,current_app
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import and_
from sqlalchemy import func,distinct
from sqlalchemy.sql import label
from models import *
import psycopg2
import twilio
from twilio.rest import TwilioRestClient
from datetime import timedelta
import json 
from functools import update_wrapper
from sqlalchemy.dialects.postgresql import TIMESTAMP
import uuid
import facebook
import json
import twitter
from collections import Counter
from prettytable import PrettyTable
import nltk
from nltk.corpus import wordnet
from weather_final import weather
import random

#from flask_oauth import OAuth
SECRET_KEY = 'development key'
#DEBUG = True
#FACEBOOK_APP_ID = '486095671495875'
#FACEBOOK_APP_SECRET = '8e1b3cd3e5ff3fb368c050d6b6191504'

#app = Flask(__name__)

#oauth = OAuth()
#app.config.from_pyfile('todoapp.cfg')
#app.config['PROPAGATE_EXCEPTIONS'] = True
#app.secret_key = SECRET_KEY
#oauth = OAuth()
#db_conn = 'postgresql+psycopg2://postgres:root@localhost/realsharedb' 
#app.config['SQLALCHEMY_DATABASE_URI'] = db_conn

app = Flask(__name__)
app.secret_key = SECRET_KEY
app.config.from_object('config')
app.debug = True
db = SQLAlchemy(app)
#db.create_all()

# engine = create_engine('postgresql://postgres:miriyala@localhost/realsharedb' , convert_unicode=True)
# metadata = MetaData(bind=engine)
# con  = engine.connect


# engine = sqlalchemy.engine_from_config(conf.local_conf, 'sqlalchemy.')
# conn = engine.connect()
# trans = conn.begin()
# try:
#     old_metatadata.tables['address'].rename('address_migrate_tmp', connection=conn)
#     new_metatadata.tables['address'].create(bind=conn)
# except:
#     trans.rollback()
#     raise
# else:
#     trans.commit()

# campaigns = Table('campaigns', metadata,
#    Column('campaign_id', Integer, primary_key=True),
#    Column('campaign_name',String(64)),
#    Column('start_date',DateTime),
#    Column('end_date',DateTime),
#    Column('budget',Integer),
#    Column('frequency_cap',Integer),
#    Column('gender',String(20)),
#    Column('income',String(20)),
#    Column('agegroup',String(20)),
#    Column('occupation',String(20)),
#    Column('race',String(20)),
#    Column('recency',String(20)),
#    Column('frequency',String(20)), 
#    Column('monthly_income',String(20)), 
#    Column('target_visitor',String(20)), 
#    Column('distance_range',String(20)), 
#    Column('mobile_sms',String(20)),
# )
# campaigns.create()


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator


'''
db_conn = 'postgresql+psycopg2://postgres:root@localhost/realsharedb' 
app = Flask(__name__) 
app.config['SQLALCHEMY_DATABASE_URI'] = db_conn
db = SQLAlchemy(app)'''

'''facebook = oauth.remote_app('facebook',
    base_url='https://graph.facebook.com/',
    request_token_url=None,
    access_token_url='/oauth/access_token',
    authorize_url='https://www.facebook.com/dialog/oauth',
    consumer_key=FACEBOOK_APP_ID,
    consumer_secret=FACEBOOK_APP_SECRET,
    request_token_params={'scope': 'email'}
) 
'''







@app.route('/')
def new1():
    return render_template('new1.html')







@app.route('/123')
def home():
    return render_template('index.html')
    


@app.route('/propertyDetail/<int:property_id>')
def propDetail(property_id):
    return render_template('property_details.html',  
        propertyAssets = db.session.query(Asset).join(Property).filter(Asset.property_id==property_id).all(),
        propertyitem = Property.query.filter_by(id=property_id).all()
    )


@app.route('/showbankers')
def showbankers():
    return render_template('showbankers.html',
        bankers=Banker.query.order_by(Banker.id).all()
    )

@app.route('/showusers')
def showusers():
    return render_template('showusers.html',
        users=User.query.order_by(User.id).all()
    )

@app.route('/showassets/<int:property_id>', methods= ['GET','POST'])
def showassets(property_id):
    return render_template('showassets.html',
        propertyId = str(property_id),
        assets = Asset.query.filter_by(property_id=property_id).all()
    )


@app.route('/showproperties')
def showproperties():
    return render_template('showproperties.html',        
        properties=Property.query.order_by(Property.id).all()
    )

@app.route('/new2', methods=['GET', 'POST'])
def new():
    if request.method == 'POST':
        if not request.form['bankName']:
            flash('Bank Name is required', 'error')
        elif not request.form['text']:
            flash('URL is required', 'error')
        else:
            todo = Todo(request.form['title'], request.form['text'])
            db.session.add(todo)
            db.session.commit()
            flash(u'Todo item was successfully created')
            return redirect(url_for('index'))
    return render_template('new.html')


@app.route('/newBanker', methods=['GET', 'POST'])
def newBanker():
    if request.method == 'POST':
        if not request.form['bankName']:
            flash('Bank Name is required', 'error')
        elif not request.form['url']:
            flash('URL is required', 'error')
        else:
            banker = Banker(request.form['bankName'], request.form['url'], request.form['bank_desc'])
            db.session.add(banker)
            db.session.commit()
            flash(u'Banker has been successfully added')
            return redirect(url_for('showbankers'))
    return render_template('newBanker.html')

@app.route('/newAsset/<int:property_id>', methods=['GET', 'POST'])
def newAsset(property_id):
    if request.method == 'POST':
        if not request.form['assetUrl']:
            flash('Asset URL is required', 'error')
        elif not request.form['assetDesc']:
            flash('Please tell us what this asset is', 'error')
        else:
            asset = Asset(request.form['assetUrl'], request.form['assetDesc'], request.form['assetType'], property_id)            
            db.session.add(asset)
            db.session.commit()
            flash(u'Asset has been successfully added')
            return redirect(url_for('showassets',property_id=property_id))
    return render_template('newAsset.html', property_id = property_id)

@app.route('/newUser', methods=['GET', 'POST'])
def newUser():
    if request.method == 'POST':
        if not request.form['last_nm']:
            flash('Last Name is required', 'error')
        elif not request.form['first_nm']:
            flash('First Name is required', 'error')
        elif not request.form['middle_nm']:
            flash('Middle Name is required', 'error')
        elif not request.form['primary_no']:
            flash('Phone number is required', 'error')
        elif not request.form['email']:
            flash('Email is required', 'error')              
        else:
            print "hello"
            user = User(request.form['last_nm'],request.form['first_nm'],\
                request.form['middle_nm'],request.form['primary_no'],request.form['email'],\
                request.form['password'],request.form['address'],request.form['user_flag'],\
                request.form['registered_name'], request.form['registration_number'])
            db.session.add(user)
            db.session.commit()
            flash(u'User has been successfully added')
            return redirect(url_for('showusers'))
    return render_template('newUser.html')

@app.route('/newProperty', methods=['GET', 'POST'])
def newProperty():
    if request.method == 'POST':
        if not request.form['property_nm']:
            flash('Last Name is required', 'error')         
        else:
            prop = Property(request.form['property_nm'])
            db.session.add(prop)
            db.session.commit()
            flash(u'Prop has been successfully added')
            return redirect(url_for('showproperties'))
    return render_template('newProperty.html')



@app.route('/assets/<int:asset_id>', methods = ['GET' , 'POST'])
def show_or_update_asset(asset_id):
    asset_item = Asset.query.get(asset_id)
    if request.method == 'GET':
        return render_template('viewassets.html',asset=asset_item)
    asset_item.asset_url = request.form['assetUrl']
    asset_item.asset_desc  = request.form['assetDesc']
    asset_item.asset_type  = request.form['assetType']
    db.session.commit()
    return redirect(url_for('showassets',property_id=asset_item.property_id))


@app.route('/bankers/<int:banker_id>', methods = ['GET' , 'POST'])
def show_or_update_Banker(banker_id):
    banker_item = Banker.query.get(banker_id)
    if request.method == 'GET':
        return render_template('viewbankers.html',banker=banker_item)
    banker_item.banker_name = request.form['title']
    banker_item.banker_logo_url  = request.form['text']
    banker_item.banker_desc  = request.form['desc']
    db.session.commit()
    return redirect(url_for('showbankers'))

@app.route('/users/<int:user_id>', methods = ['GET' , 'POST'])
def show_or_update_User(user_id):
    user_item = User.query.get(user_id)
    if request.method == 'GET':
        return render_template('viewusers.html',user=user_item)
    user_item.last_nm = request.form['last_nm']
    user_item.first_nm  = request.form['first_nm']
    user_item.middle_nm  = request.form['middle_nm']
    user_item.primary_no  = request.form['primary_no']
    user_item.email  = request.form['email']
    user_item.password  = request.form['password']
    user_item.address  = request.form['address']
    user_item.user_flag  = request.form['user_flag']
    user_item.registered_name  = request.form['registered_name']
    user_item.registration_number  = request.form['registration_number']
    db.session.commit()
    return redirect(url_for('showusers'))

@app.route('/properties/<int:property_id>', methods = ['GET' , 'POST'])
def show_or_update_Property(property_id):
    property_item = Property.query.get(property_id)
    if request.method == 'GET':
        return render_template('viewproperties.html',propertyitem=property_item)
    property_item.property_nm = request.form['property_nm']
    property_item.property_desc  = request.form['property_desc']
    property_item.property_building_progress  = request.form['property_building_progress']
    property_item.property_funding_progress  = request.form['property_funding_progress']
    property_item.returns  = request.form['returns']
    property_item.fee  = request.form['fee']
    db.session.commit()
    return redirect(url_for('showproperties'))



@app.route('/users/<string:groupColumn>',methods = ['POST','GET','OPTIONS'])
@crossdomain(origin="*",headers="Content-Type")
def testUsers(groupColumn):
    #category_type = request.get.key("category")
  #dataresult = db.session.query(func.age_gr,func.count(func.age_gr)).group_by(func.age_gr).all()  
    mapDict = {"id":'user_id',"device":'user_device',"age":'user_age',"gender":'user_gender',"income":'user_income',"occupation":'user_occupation',"race":'user_race',"date":'user_date'}   
    if mapDict.has_key(groupColumn):
      print mapDict[groupColumn]    
    sqlalchvar = mapDict[groupColumn]
    #User.query.filter_by(username='peter').first()
    dataresult = db.session.query(getattr(UserData, sqlalchvar), func.count(getattr(UserData, sqlalchvar))).group_by(getattr(UserData, sqlalchvar)).all()
    #dataresult = db.session.query(getattr(UserData,sqlalchvar)).filter(UserData.user_device =='Android').count()
    #s = db.session.query(User.id).filter(User.nickname.like("%a%")).subquery()
    #dataresult = db.session.query(getattr(UserData,sqlalchvar)).filter(UserData.user_occupation.like("%R%")).first()
    #dataresult = db.session.query(getattr(UserData,sqlalchvar)).filter(and_(UserData.user_device == 'Android',UserData.user_income=='H')).count()
    #dataresult = db.session.query(getattr(UserData,sqlalchvar)),UserData.user_device.match('Android')
    #query.filter(User.name.match('wendy'))
    #dataresult = db.session.query(getattr(UserData,sqlalchvar)),func.count('*').select_from(UserData.user_age).scalar()
    #session.query(func.count(User.id)).scalar()
    #session.query(func.count('*')).select_from(User).scalar() 
    #query.filter(and_(User.name == 'ed', User.fullname == 'Ed Jones'))
    #session.query(func.count(distinct(User.name)))
    data = json.dumps(dataresult)
    user = json.loads(data)
    print user
    return render_template('display.html',resultlist =user)


@app.route('/projects/highpoverty/states')
def high_poverty_states():
    donors_choose_url = "http://api.donorschoose.org/common/json_feed.html?highLevelPoverty=true&APIKey=DONORSCHOOSE"
    response = urllib2.urlopen(donors_choose_url)
    json_response = json.load(response)
    states = set()
    for proposal in json_response["proposals"]:
        states.add(proposal["state"])
    return json.dumps(list(states))

    
@app.route('/segments/<string:segmentsColumns>',methods = ['POST','GET','OPTIONS'])
@crossdomain(origin="*",headers="Content-Type")
def  testSegments(segmentsColumns):
    mapDict = {"name":' segment_name ',"age":'segment_age',"occupation":'segement_occupation',"income":'segment_income',"race":'segment.race'}
    if mapDict.has_key(segmentsColumns):
        print mapDict[segmentsColumns]
    segmentdata = mapDict[segmentsColumns]
    segmentresult = db.session.query(getattr(Segment,segmentdata),func.count(getattr(Segment,segmentdata))).group_by(getattr(Segment,segmentdata)).all()
    return json.dumps(segmentresult)


@app.route('/camps/<string:campaignColumn>',methods = ['POST','GET','OPTIONS'])
@crossdomain(origin="*",headers="Content-Type")
def testCamps(campaignColumn):
    mapDict ={"name":'campaign_name',"banner":'campaign_banner',"mobile":'campaign_mobile',"sms":'campaign_sms'}
    if mapDict.has_key(campaignColumn):
        print mapDict[campaignColumn]
    campaigndata = mapDict[campaignColumn]
    #campaignresult = db.session.query(getattr(Campaign,campaigndata),func.count(getattr(Campaign,campaigndata))).group_by(getattr(Campaign,campaigndata)).all()
    campaignresult = db.session.query(getattr(Campaign,campaigndata)).all()
    data = json.dumps(campaignresult)
    resultdata = json.loads(data)
    print resultdata
    return render_template('campaigntree.html',resultlist = resultdata)
    


@app.route('/traf/<string:trafficColumns>',methods = ['POST','GET','OPTIONS'])
@crossdomain(origin="*",headers="Content-Type")
def testTraffic(trafficColumns):
    mapDict = {"id":'user_i_id',"userid":'user_id',"visit":'visit_at'}
    if mapDict.has_key(trafficColumns):
        print mapDict[trafficColumns]
    trafficdata = mapDict[trafficColumns]
    trafficresult = db.session.query(getattr(UserTraffic,trafficdata),func.count(getattr(UserTraffic,trafficdata))).group_by(getattr(UserTraffic,trafficdata)).all()
    return json.dumps(trafficresult)



# @app.route('/campassets',methods = ['POST','GET','OPTIONS'])
# @crossdomain(origin="*",headers="Content-Type")
# def testCampAssets():
#     #d = db.session.query(CampaignData).join(CampaignAssert).filter(CampaignData.id == CampaignAssert.id).all()
#     query = db.session.query(CampaignData.id,CampaignData.campaign_name,CampaignData.campaign_assertid,CampaignAssert.assert_type,CampaignAssert.asserts_clicks,CampaignAssert.asserts_views).join(CampaignAssert,CampaignData.id == CampaignAssert.id).all()
#     #query = db.session.query(CampaignData).all()
#     object_list=[]
#     for campaign in query:
#         #print campaign.id, campaign.campaign_name, campaign.assert_type
#         #print campaign.id
#         assets = db.session.query(CampaignAssert).filter(campaign.id==CampaignAssert.campaign_id).all()

#         for asset in assets:
#             #print
#             #print "----------------"
#             # campId= campaign.id
#             # assetId =  asset.id
#             campaignName = str(campaign.campaign_name)
#             assetType = str(asset.assert_type)
#             assetClicks = str(asset.asserts_clicks)
#             assetViews = str(asset.asserts_views)
#             flag=0
#             for dic in object_list:
#                 if campaignName == dic['campaigname']:
#                     d1={}
#                     d1['assettype'] = assetType
#                     d1['assetclicks'] = assetClicks
#                     d1['assetviews'] = assetViews
#                     dic['campaigndetails'].append(d1)
#                     flag=1
#                     break
#             if(flag==0):      
#                 d={}
#                 d1={} 
#                 d['campaigname'] = campaignName
#                 d['campaigndetails']=[]
#                 d1['assettype'] = assetType
#                 d1['assetclicks'] = assetClicks
#                 d1['assetviews'] = assetViews
#                 d['campaigndetails'].append(d1)
#                 object_list.append(d)
#     data = json.dumps({'data': object_list})
#     """for i in object_list:
#         print i['campaigname']
#         for j in i['campaigndetails']:
#             print j['asserts_clicks']"""
#     return data
    
    # for asset in assets:
    # print assert_type
    #d = {} 
    # object_list=[]
    # for u in query:
    #  result = u.__dict__ 
    #  print result
    #  object_list.append(result)
    #  print object_list
    # d['resp'] = object_list      
  
   
    # return render_template('example1.html',resultdata = jsondata)
     # campaigns = db.session.query(Campaign).filter(Campaign.campaign_segment==str(sg.id)).all()
     #                    for cp in campaigns:
     #                        print "xxxx"
     #                        print cp.id                    
     #                        assets = db.session.query(Asset).filter(Asset.campaign_id==str(cp.id)).all()
     #                        for asset in assets:
     #                            print "^^^^^^^^^^^^^^^^^^^^^^^^^^^"
     #                            print asset.id
     #                            print asset.asset_url
     #                            print "^^^^^^^^^^^^^^^^^^^^^^^^^^^"
     #                        account_sid = "AC45ae2390277dc6e9a40efdfcc2b9b953"
     #                        auth_token  = "9866b1807740eea22c073e562396c8d2"
     #                        assetId = str(assets[0].id)
     #                        campaignUrl = str(assets[0].asset_url)
     #                        finaltxt = "Idisplay SMS : "+str(assets[0].asset_txt)+" visit "+str(assets[0].asset_url)
     #                        print finaltxt
     #                        print "phone is:"+phone_ 
     #                        # client = TwilioRestClient(account_sid, auth_token)
     #                        # try:
     #                        #     client = twilio.rest.TwilioRestClient(account_sid, auth_token)                                               
     #                        #     message = client.sms.messages.create(
     #                        #         body=finaltxt,
     #                        #         to=phone_,
     #                        #         from_="+19524666677"
     #                        #         )
     #                        #     print message.sid                    
     #                        # except twilio.TwilioRestException as e:
     #                        #     print e                                        
     #                        print "xxxx"
     #                    print "=========================="   
     #                    #print uuid.uuid4()
     #                    banner_id = str(uuid.uuid4())
     #                    objects_list=[]
     #                    d={}
     #                    d['userId']=userid_
     #                    d['phoneNo']= phone_
     #                    d['IMEI'] = imei_
     #                    d['segmentId'] = sg.id
     #                    d['campaignId'] = cp.id
     #                    d['promotionText'] = finaltxt
     #                    d['promotionUrl']=campaignUrl
     #                    d['bannerId'] = banner_id
     #                    d['redirectUrl'] = 'http://dres.sy'
     #                    d['assetId']=assetId
     #                    objects_list.append(d)
     #                    js= json.dumps({'kind': 'results','data' : {'modhash':'','children':objects_list}})
     #                    return js


@app.route('/showcampaigns')
def showcampaigns():
    return render_template('showcampaigns.html',
        campaigs = Campaigns.query.order_by(Campaigns.id).all()
    )


@app.route('/campaigns', methods = ['GET' , 'POST'])
def campaigns():
    if request.method == 'POST':
        print "Hello"
        if not request.form['campaign_name']:
            flash('campaignname is required', 'error') 
        elif not request.form['start_date']:
            flash('StartDate is required', 'error')
        elif not request.form['end_date']:
            flash('EndDate is required', 'error')
        elif not request.form['budget']:
            flash('Budget is required','error')
        elif not request.form['frequency_cap']:
            flash('frequencycap is required','error')
        else:
            camps = Campaigns(request.form['campaign_name'],request.form['start_date'], \
            request.form['end_date'],request.form['budget'],request.form['frequency_cap'],\
            request.form['gender'],request.form['income'],request.form['agegroup'] ,\
            request.form['occupation'], request.form['race'], request.form['recency'] ,\
            request.form['frequency'], request.form['monthly_income'], request.form['target_visitor'] ,\
            request.form['distance_range'], request.form['mobile_sms'],request.form['banner'],request.form['video']) 
            print "bye"
            db.session.add(camps)
            db.session.commit()
            flash(u'Prop has been successfully added')
            return redirect(url_for('showcampaigns'))   
    return render_template('camp.html')




@app.route('/store1',methods = ['GET','POST'])
def store1():
    return render_template('store1.html')

@app.route('/test',methods = ['GET','POST'])
def test():
    return render_template('test.html')


	 
	

@app.route('/storedata',methods = ['POST','GET','OPTIONS'])
def storedata():
    if request.method == 'POST':
        print request.data
    postedJson = json.loads(request.data)
    storeId = str(postedJson['storeId'])
    startDate = str(postedJson['startDate'])
    endDate = str(postedJson['endDate'])
    day_flag = int(postedJson['day_flag'])
    diff_days = int(postedJson['diff_days'])
    store={}
    store1={}
    con = psycopg2.connect("host=localhost user=postgres password=mohanrao dbname=realsharedb")
    cur = con.cursor()
    station_list=""
    if(storeId == "all"):
    	cur.execute("select store_id from store group by store_id")
	stations=cur.fetchall()
	for i in stations:
		station_list += ("'"+i[0] + "',")
	station_list=station_list[:-1]
    else:
     	storeId=storeId.split(',')
	for i in storeId:
		station_list += ("'"+i+"',")
     	station_list = station_list[:-1]
    station_list = "("+station_list+")"

    print station_list 
    if(day_flag==0):
    	cur.execute(" select store_id,store_date,SUM(CASE WHEN  store_gender='M' THEN 1 ELSE 0 END )AS Male,SUM(CASE WHEN store_gender='F' THEN 1 ELSE 0 END )AS Female,SUM(CASE WHEN store_age = 'adult' THEN 1 ELSE 0 END)AS adult,SUM(CASE WHEN store_age = 'senior' THEN 1 ELSE 0 END)AS senior,SUM(CASE WHEN store_age = 'junior' THEN 1 ELSE 0 END)AS junior,SUM(CASE WHEN store_age = 'young-adult' THEN 1 ELSE 0 END)AS young_adult From store where store_date between '"+startDate+"' and '"+endDate+"' and store_id in "+station_list+" group by store_date,store_id")
	rows=cur.fetchall()	
	"""if(len(cur.fetchall()) == 0):
    	    cur1 = con.cursor()
    	    cur1.execute(" select store_id From store group by store_id")
	    store_id=cur1.fetchall()
	    for i in store_id:
	        store2['graph_data']={}
		store1[i[0]]={"male_count":0 , "female_count":0 , "senior":0 , "young-adult":0 , "junior":0 , "adult":0 , "graph_data":store}"""
    else:
    	cur.execute(" select store_id,store_hour,SUM(CASE WHEN  store_gender='M' THEN 1 ELSE 0 END )AS Male,SUM(CASE WHEN store_gender='F' THEN 1 ELSE 0 END )AS Female,SUM(CASE WHEN store_age = 'adult' THEN 1 ELSE 0 END)AS adult,SUM(CASE WHEN store_age = 'senior' THEN 1 ELSE 0 END)AS senior,SUM(CASE WHEN store_age = 'junior' THEN 1 ELSE 0 END)AS junior,SUM(CASE WHEN store_age = 'young-adult' THEN 1 ELSE 0 END)AS young_adult From store where store_date between '"+startDate+"' and '"+endDate+"'  and store_id in "+station_list+" group by store_hour,store_id")
	rows=cur.fetchall()	
	"""if(len(cur.fetchall()) == 0):
    	    cur1 = con.cursor()
    	    cur1.execute(" select store_id From store group by store_id")
	    store_id=cur1.fetchall()
	    for i in store_id:
	        store2['graph_data']={}
		store1[i[0]]={"male_count":0 , "female_count":0 , "senior":0 , "young-adult":0 , "junior":0 , "adult":0 , "graph_data":store}"""
    
    store_id=[]
    for i in rows:
	store[str(i[1])] = { 'male_count':int(i[2]), 'adult':int(i[4]), 'female_count':int(i[3]), 'senior':int(i[5]), 'young-adult':int(i[7]), 'junior':int(i[6])}
	if(i[0] not in store_id):
		store1[i[0]]={}
		store1[i[0]]['male_count'] = int(i[2])
		store1[i[0]]['adult'] = int(i[4])
		store1[i[0]]['female_count'] = int(i[3])
		store1[i[0]]['senior'] =  int(i[5])
		store1[i[0]]['young-adult'] = int(i[7])
		store1[i[0]]['junior'] = int(i[6])
		store_id.append(i[0])
        else:
		store1[i[0]]['male_count'] += int(i[2])
		store1[i[0]]['adult'] += int(i[4])
		store1[i[0]]['female_count'] += int(i[3])
		store1[i[0]]['senior'] += int(i[5])
		store1[i[0]]['young-adult'] += int(i[7])
		store1[i[0]]['junior'] += int(i[6])
	store1[i[0]]['graph_data']=store
    data = json.dumps({'data':store1})
    print data
    return data
   




@app.route('/proof_station',methods = ['GET','POST','OPTIONS'])
def proof_station():
	station_names=db.session.query(Proof.proof_station).group_by(Proof.proof_station).all()
	station_name=[]
	for i in station_names:
		station_name.append(str(i.proof_station))
	return json.dumps({'data':station_name})

@app.route('/store_id',methods = ['GET','POST','OPTIONS'])
def store_id():
	store_ids=db.session.query(Store.store_id).group_by(Store.store_id).all()
	store_id=[]
	for i in store_ids:
		store_id.append(str(i.store_id))
	return json.dumps({'data':store_id})


def get_promo():
	query=db.session.query(Proof.proof_package).group_by(Proof.proof_package).all()
	dic={}
	for i in query:
		dic[str(i.proof_package)]=[]
		query1=db.session.query(Proof.proof_content).filter(Proof.proof_package == i.proof_package).group_by(Proof.proof_content).all()
		for j in query1:
			promo=j.proof_content
			dic[str(i.proof_package)].append(str(promo))
	print 'dic ======================================' , dic
	return dic

def parse_brand_store(results):
	dic={}
	for i in results:
	    station=str(i[0])
	    brand=str(i[1])
	    count=(i[2])
	    if(station not in dic.keys()):
		dic[station]={}
	    dic[station][brand]=count
	return dic

def get_Brandnames():
	brand_name=[]
	brand_names=db.session.query(Proof.proof_package).group_by(Proof.proof_package).all()
	for i in brand_names:
	    	brand_name.append(i.proof_package)
	return brand_name

def parse_brand_time(results):
	dic={}
	for i in results:
		brand=str(i[0])
		time=str(i[1])
		count=i[2]
		if(brand not in dic.keys()):
			dic[brand]={}
		dic[brand][time]=count
	return dic



def parse_promoVstore(results,brand_promotions):
	dic={}
	for i in results:
		brand=str(i[0])
		promo=str(i[1])
		station=str(i[2])
		count=i[3]
		if(brand not in dic.keys()):
			dic[brand]={}
			dic[brand]['store_data']={}
		if(station not in dic[brand]['store_data'].keys()):
			dic[brand]['store_data'][station]={}
		dic[brand]['store_data'][station][promo]=count
		dic[brand]['promotion_names']=brand_promotions[brand]
	return dic

def parse_promoVtime(results,brand_promotions):
	dic={}
	for i in results:
		brand=str(i[0])
		promo=str(i[1])
		time=str(i[2])
		count=i[3]
		if(brand not in dic.keys()):
			dic[brand]={}
		if(promo not in dic[brand].keys()):
			dic[brand][promo]={}
		dic[brand][promo][time]=count
	return dic

@app.route('/proofdata',methods = ['POST','GET','OPTIONS'])
def proofdata():
     if request.method == 'POST':
	print request.data
	postedJson = json.loads(request.data)
     	storeId = str(postedJson['storeId'])
     	startDate = str(postedJson['startDate'])
     	endDate = str(postedJson['endDate'])
	day_flag = int(postedJson['day_flag'])
	diff_days = int(postedJson['diff_days'])
     proof_data={}
     proof_data1={}
     proof_Data_brandVstore={}
     proof_Data_brandVtime={}
     dic={}
     brand_promotions=get_promo()
     con = psycopg2.connect("host=localhost user=postgres password=mohanrao dbname=realsharedb")
     cur = con.cursor()
     station_list=""
     if(storeId == "all"):
     	cur.execute("select proof_station from proof group by proof_station")
	stations=cur.fetchall()
	for i in stations:
		station_list += ("'"+i[0] + "',")
	station_list=station_list[:-1]
     else:
     	storeId=storeId.split(',')
	for i in storeId:
		station_list += ("'"+i+"',")
     	station_list = station_list[:-1]
     station_list = "("+station_list+")"


     if(1):
	cur.execute("select proof_station,proof_package,SUM(proof_duration1)AS duration from proof where proof_date between '"+startDate+"' and '"+endDate+"' and proof_station in "+station_list+" group by proof_station,proof_package")
	results=cur.fetchall()
	dic=parse_brand_store(results)
	proof_Data_brandVstore['brand_data']=dic
	brand_name=get_Brandnames()
	proof_Data_brandVstore['brand_names']=brand_name
	print proof_Data_brandVstore
	if(day_flag == 0 ):
     	     cur.execute("select proof_package,proof_date, SUM(proof_duration1) AS duration From proof where proof_date between '"+startDate+"' and '"+endDate+"' and proof_station in "+station_list+" group by proof_package,proof_date order by proof_package,proof_date")
	     results_brandVtime=cur.fetchall()
	     proof_Data_brandVtime=parse_brand_time(results_brandVtime)

	     cur.execute("select proof_package,proof_content,proof_station, SUM(proof_duration1) AS duration From proof where proof_date between '"+startDate+"' and '"+endDate+"' and proof_station in "+station_list+" group by proof_package,proof_content,proof_station order by proof_package,proof_station,proof_content;")
	     results_promoVstore=cur.fetchall()
	     proof_Data_promoVstore=parse_promoVstore(results_promoVstore,brand_promotions)
	     
	     cur.execute("select proof_package,proof_content,proof_date,SUM(proof_duration1) AS duration From proof where proof_date between '"+startDate+"' and '"+endDate+"' and proof_station in "+station_list+" group by proof_package,proof_content,proof_date order by proof_package,proof_content,proof_date")
	     results_promoVtime=cur.fetchall()
	     proof_Data_promoVtime=parse_promoVtime(results_promoVtime,brand_promotions)
	else:
     	     cur.execute("select proof_package,proof_hour, SUM(proof_duration1) AS duration From proof where proof_date between '"+startDate+"' and '"+endDate+"' and proof_station in "+station_list+" group by proof_package,proof_hour order by proof_package,proof_hour;")
	     results_brandVtime=cur.fetchall()
	     proof_Data_brandVtime=parse_brand_time(results_brandVtime)
	     print proof_Data_brandVtime

	     cur.execute("select proof_package,proof_content,proof_station, SUM(proof_duration1) AS duration From proof where proof_date between '"+startDate+"' and '"+endDate+"' and proof_station in "+station_list+" group by proof_package,proof_content,proof_station order by proof_package,proof_station,proof_content;")
	     results_promoVstore=cur.fetchall()
	     proof_Data_promoVstore=parse_promoVstore(results_promoVstore,brand_promotions)
	     
	     cur.execute("select proof_package,proof_content,proof_hour, SUM(proof_duration1) AS duration From proof where proof_date between '"+startDate+"' and '"+endDate+"' and proof_station in "+station_list+" group by proof_package,proof_content,proof_hour order by proof_package,proof_content,proof_hour;")
	     results_promoVtime=cur.fetchall()
	     proof_Data_promoVtime=parse_promoVtime(results_promoVtime,brand_promotions)
		
     data = json.dumps({"data_store":proof_Data_brandVstore,"data_time":proof_Data_brandVtime,"promoVstore":proof_Data_promoVstore,"promoVtime":proof_Data_promoVtime}) 
     return data



def parse_durationVstore(results):
	dic={}
	station_check=""
	for i in results:
		station=str(i[0])
		brand=str(i[1])
		total_count=int(i[2])
		brand_count=int(i[3])
		if(station_check != station):
			station_check=station
			dic[station]={}
			dic[station]['total_view']=total_count
		dic[station][brand]=brand_count
	return dic
def get_brandnames(brandnames):
	brand_names=[]
	for i in brandnames:
		brand_names.append(str(i[0]))
	return brand_names

def parse_countVstore(results):
	dic={}
	station_check=""
	for i in results:
		station=str(i[0])
		brand=str(i[1])
		total_view=float(i[2])
		brand_duration=float(i[3])
		if(station_check != station):
			station_check=station
			dic[station]={}
			dic[station]['total_view']=total_view
		dic[station][brand]=brand_duration
	return dic
def parse_durationVduration(results):
	dic={'total_view':{}}
	for i in results:
		brand=str(i[0])
		date=str(i[1])
		total_view=float(i[2])
		campaign_time=float(i[3])
		if(brand not in dic.keys()):
			dic[brand]={}
		dic[brand][date]=campaign_time
		dic['total_view'][date]=total_view
	return dic

def parse_countVduration(results):
	dic={'total_view':{}}
	for i in results:
		brand=str(i[0])
		date=str(i[1])
		total_count=int(i[2])
		campaign_count=int(i[3])
		if(brand not in dic.keys()):
			dic[brand]={}
		dic[brand][date]=campaign_count
		dic['total_view'][date]=total_count
	return dic
		

@app.route('/visualdata',methods = ['POST','GET','OPTIONS'])
def visualdata():
	if request.method == 'POST':
		print request.data
		postedJson = json.loads(request.data)
	     	storeId = str(postedJson['storeId'])
	     	startDate = str(postedJson['startDate'])
	     	endDate = str(postedJson['endDate'])
		day_flag = int(postedJson['day_flag'])
		diff_days = int(postedJson['diff_days'])
	con = psycopg2.connect("host=localhost user=postgres password=mohanrao dbname=realsharedb")
	cur = con.cursor()
	station_list=""
	cur.execute("select proof_package from proof group by proof_package")
	brandnames=cur.fetchall()
	proof_Data_durationVstore={'data':{},'brand_names':{}}
	proof_Data_countVstore={'data':{},'brand_names':{}}
	brand_names=get_brandnames(brandnames)
	if(storeId == "all"):
		cur.execute("select proof_station from proof group by proof_station")
		stations=cur.fetchall()
		for i in stations:
			station_list += ("'"+i[0] + "',")
		station_list=station_list[:-1]
	else:
		storeId=storeId.split(',')
		for i in storeId:
			station_list += ("'"+i+"',")
     		station_list = station_list[:-1]
     	station_list = "("+station_list+")"
	if(day_flag==0):
		cur.execute("WITH store_temp as (select store_id,store_date,SUM(store_dwelltime1) AS duration From store where store_date between '2014-06-01' and '2014-06-10' and store_id in "+station_list+" group by store_id,store_date order by store_id,store_date),proof_temp as (select proof_station,proof_package,proof_date,SUM(proof_duration1)AS campaign_time from proof where proof_date between '2014-06-01' and '2014-06-10' and proof_station in "+station_list+" group by proof_station,proof_package,proof_date order by proof_station,proof_package)  select store_temp.store_id,proof_temp.proof_package,SUM(store_temp.duration),SUM(proof_temp.campaign_time) from store_temp INNER JOIN proof_temp on (store_temp.store_id = proof_temp.proof_station and store_temp.store_date = proof_temp.proof_date) group by store_temp.store_id,proof_temp.proof_package;")
		results_durationVstore=cur.fetchall()
	        proof_Data_durationVstore['data']=parse_durationVstore(results_durationVstore)
	 	proof_Data_durationVstore['brand_names']=brand_names



	 	cur.execute("WITH store_temp as (select store_id,store_date,SUM(store_dwelltime1) AS duration From store where store_date between '2014-06-01' and '2014-06-10' and store_id in "+station_list+" group by store_id,store_date order by store_id,store_date),proof_temp as (select proof_station,proof_package,proof_date,SUM(proof_duration1)AS campaign_time from proof where proof_date between '2014-06-01' and '2014-06-10' and proof_station in "+station_list+" group by proof_station,proof_package,proof_date order by proof_station,proof_package)  select proof_temp.proof_package,store_date,SUM(store_temp.duration) as total_viewership,SUM(proof_temp.campaign_time) as campaign_time from store_temp INNER JOIN proof_temp on (store_temp.store_id = proof_temp.proof_station and store_temp.store_date = proof_temp.proof_date) group by store_temp.store_date,proof_temp.proof_package order by proof_package,store_date;")
		results_durationVduration=cur.fetchall()
		proof_Data_durationVduration=parse_durationVduration(results_durationVduration)
		print proof_Data_durationVduration

		cur.execute("WITH store_temp as (select store_id,store_date,count(*) AS count From store where store_date between '2014-06-01' and '2014-06-10' and store_id in "+station_list+" group by store_id,store_date order by store_id,store_date),proof_temp as (select proof_station,proof_package,proof_date,SUM(proof_count) AS count1 from proof where proof_date between '2014-06-01' and '2014-06-10' and proof_station in "+station_list+" group by proof_station,proof_package,proof_date order by proof_station,proof_package)  select store_temp.store_id,proof_temp.proof_package,SUM(store_temp.count),SUM(proof_temp.count1) from store_temp INNER JOIN proof_temp on (store_temp.store_id = proof_temp.proof_station and store_temp.store_date = proof_temp.proof_date) group by store_temp.store_id,proof_temp.proof_package;")
		results_countVstore=cur.fetchall()
		proof_Data_countVstore['data']=parse_countVstore(results_countVstore)
		proof_Data_countVstore['brand_names']=brand_names


		cur.execute("WITH store_temp as (select store_id,store_date,count(*) AS count From store where store_date between '2014-06-01' and '2014-06-10' and store_id in "+station_list+" group by store_id,store_date order by store_id,store_date),proof_temp as (select proof_station,proof_package,proof_date,SUM(proof_count) AS count1 from proof where proof_date between '2014-06-01' and '2014-06-10' and proof_station in "+station_list+" group by proof_station,proof_package,proof_date order by proof_station,proof_package)  select proof_temp.proof_package,store_date,SUM(store_temp.count) as total_viewership_count,SUM(proof_temp.count1) as campaign_count from store_temp INNER JOIN proof_temp on (store_temp.store_id = proof_temp.proof_station and store_temp.store_date = proof_temp.proof_date) group by store_temp.store_date,proof_temp.proof_package order by proof_package,store_date;")
		results_countVduration=cur.fetchall()
		proof_Data_countVduration=parse_countVduration(results_countVduration)

	else:
		cur.execute("WITH store_temp as (select store_id,store_hour,SUM(store_dwelltime1) AS duration From store where store_date between '2014-06-06' and '2014-06-06' and store_id in "+station_list+" group by store_id,store_hour order by store_id,store_hour),proof_temp as (select proof_station,proof_package,proof_hour,SUM(proof_duration1)AS campaign_time from proof where proof_date between '2014-06-06' and '2014-06-06' and proof_station in "+station_list+"  group by proof_station,proof_package,proof_hour order by proof_station,proof_package)  select store_temp.store_id,proof_temp.proof_package,SUM(store_temp.duration),SUM(proof_temp.campaign_time) from store_temp INNER JOIN proof_temp on (store_temp.store_id = proof_temp.proof_station and store_temp.store_hour = proof_temp.proof_hour) group by store_temp.store_id,proof_temp.proof_package;")
		results_durationVstore=cur.fetchall()
	        proof_Data_durationVstore['data']=parse_durationVstore(results_durationVstore)
	 	proof_Data_durationVstore['brand_names']=brand_names

		cur.execute("WITH store_temp as (select store_id,store_hour,SUM(store_dwelltime1) AS duration From store where store_date between '2014-06-06' and '2014-06-06' and store_id in "+station_list+" group by store_id,store_hour order by store_id,store_hour),proof_temp as (select proof_station,proof_package,proof_hour,SUM(proof_duration1)AS campaign_time from proof where proof_date between '2014-06-06' and '2014-06-06' and proof_station in "+station_list+" group by proof_station,proof_package,proof_hour order by proof_station,proof_package)  select proof_temp.proof_package,store_hour,SUM(store_temp.duration) as total_viewership,SUM(proof_temp.campaign_time) as campaign_time from store_temp INNER JOIN proof_temp on (store_temp.store_id = proof_temp.proof_station and store_temp.store_hour = proof_temp.proof_hour) group by store_temp.store_hour,proof_temp.proof_package order by proof_package,store_hour")
		results_durationVduration=cur.fetchall()
		proof_Data_durationVduration=parse_durationVduration(results_durationVduration)
		print proof_Data_durationVduration

		cur.execute("WITH store_temp as (select store_id,store_hour,count(*) AS count From store where store_date between '2014-06-06' and '2014-06-06' and store_id in "+station_list+" group by store_id,store_hour order by store_id,store_hour),proof_temp as (select proof_station,proof_package,proof_hour,SUM(proof_count) AS count1 from proof where proof_date between '2014-06-06' and '2014-06-06' and proof_station in "+station_list+" group by proof_station,proof_package,proof_hour order by proof_station,proof_package)  select store_temp.store_id,proof_temp.proof_package,SUM(store_temp.count),SUM(proof_temp.count1) from store_temp INNER JOIN proof_temp on (store_temp.store_id = proof_temp.proof_station and store_temp.store_hour = proof_temp.proof_hour) group by store_temp.store_id,proof_temp.proof_package;")
		results_countVstore=cur.fetchall()
		proof_Data_countVstore['data']=parse_countVstore(results_countVstore)
		proof_Data_countVstore['brand_names']=brand_names
	
		cur.execute("WITH store_temp as (select store_id,store_hour,count(*) AS count From store where store_date between '2014-06-06' and '2014-06-06' and store_id in "+station_list+" group by store_id,store_hour order by store_id,store_hour),proof_temp as (select proof_station,proof_package,proof_hour,SUM(proof_count) AS count1 from proof where proof_date between '2014-06-06' and '2014-06-06' and proof_station in "+station_list+" group by proof_station,proof_package,proof_hour order by proof_station,proof_package)  select proof_temp.proof_package,store_hour,SUM(store_temp.count) as total_viewership_count,SUM(proof_temp.count1) as campaign_count from store_temp INNER JOIN proof_temp on (store_temp.store_id = proof_temp.proof_station and store_temp.store_hour = proof_temp.proof_hour) group by store_temp.store_hour,proof_temp.proof_package order by proof_package,store_hour;")
		results_countVduration=cur.fetchall()
		proof_Data_countVduration=parse_countVduration(results_countVduration)


	return json.dumps({'graph1':proof_Data_durationVstore,'graph2':proof_Data_durationVduration,'graph4':proof_Data_countVstore,'graph5':proof_Data_countVduration})


def get_stores(results):
	stores=[]
	for i in results:
		stores.append(str(i[0]))
	return stores

def parse_weather_results(data1,results_count):
	weather_results={}
	for i in data1:
	     weather_results[str(i)]=[data1[i]]
	     dic={}
	     for j in results_count:
	     	if(str(j[0]) == str(i)):
			dic[str(j[1])]=int(j[2])
	     weather_results[str(i)].append(dic)
	return weather_results


@app.route('/weatherdata',methods = ['POST' , 'GET' , 'OPTIONS'])
def weatherdata():
    if request.method == 'POST':
        print request.data
    postedJson = json.loads(request.data)
    storeId = str(postedJson['storeId'])
    startDate = str(postedJson['startDate'])
    endDate = str(postedJson['endDate'])
    day_flag = int(postedJson['day_flag'])
    diff_days = int(postedJson['diff_days'])
    	
    wdata=weather()
    a1=wdata.obtaining_data("hyderabad,india",startDate,endDate)
    if(day_flag == 0):  
    	results=wdata.temparatures(a1)
    	data1 = wdata.data_average(startDate,endDate,results)
    else:
	    results=wdata.temparatures1(a1)
	    data1=results
    con = psycopg2.connect("host=localhost user=postgres password=mohanrao dbname=realsharedb")
    cur = con.cursor()
    cur.execute('select store_id from store group by store_id')
    result_stores=cur.fetchall()
    stores=get_stores(result_stores)
    weather_results={}
    station_list=""
    if(storeId == 'all'):
	    for i in stores:
	    	station_list += "'"+i+"',"
	    station_list=station_list[:-1]
	    station_list = "("+station_list+")"
    else:
	storeId=storeId.split(',')
	for i in storeId:
	    station_list += "'"+i+"',"
	station_list=station_list[:-1]
	station_list = "("+station_list+")"
    if(day_flag == 0):
	    cur.execute("select store_date,store_id,SUM(CASE WHEN store_gender='M' or store_gender='F' THEN 1 ELSE 0 END)AS count from store where store_date between '"+startDate+"' and '"+endDate+"' and store_id IN "+station_list+" group by store_date,store_id order by store_date")
    else:
   	 cur.execute("select store_hour,store_id,SUM(CASE WHEN store_gender='M' or store_gender='F' THEN 1 ELSE 0 END)AS count from store where store_date between '"+startDate+"' and '"+endDate+"' and store_id IN "+station_list+" group by store_hour,store_id order by store_hour")
    results_count = cur.fetchall()
    weather_results=parse_weather_results(data1,results_count)
    weather_results1={}
    if(storeId == 'all'):
	    for i in weather_results:
	    	weather_results1[i]=[weather_results[i][0],{}]
		count=0
	    	for j in weather_results[i][1]:
			count += weather_results[i][1][j]
		if(count!=0):
			weather_results1[i][1]['all']=count
    	    weather_results=weather_results1
    print weather_results
    data = json.dumps({'data':weather_results})
    return data
	


@app.route('/twitterpage',methods = ['POST','GET','OPTIONS'])
@crossdomain(origin="*",headers="Content-Type")
def twitterpage():    
    CONSUMER_KEY = '72MHkiotkJ1wp6T5QWcSA'
    CONSUMER_SECRET = 'uARrXr52UZB0gTDPWkXENZoyjfQI5lmMu4eNloU'
    OAUTH_TOKEN = '1904683502-R0wHCglhGG2Kz3qZvWkXyqgidU68EzSGEaPD4a5'
    OAUTH_TOKEN_SECRET = 'rnl0NXHuXRi2flIl6qNMH5pcVEbddV89ug4qhsvMJM'

    auth = twitter.oauth.OAuth(OAUTH_TOKEN, OAUTH_TOKEN_SECRET,
                               CONSUMER_KEY, CONSUMER_SECRET)
    brandToCheck = ""
    if request.method == 'POST':
        print "Fws"
        print request.data
        postedJson = json.loads(request.data)
        brandToCheck = str(postedJson['brandname'])


    twitter_api = twitter.Twitter(auth=auth)
    q = brandToCheck
  #  q='levis'
    count = 100
    search_results = twitter_api.search.tweets(q=q, count=count)
    statuses = search_results['statuses']
    print "-------------------------------------------------------"

    for _ in range(5):
        print "Length of statuses", len(statuses)
        try:
            next_results = search_results['search_metadata']['next_results']
        except KeyError, e:
            break


    """print "--- next_results string ----"
    print next_results
    print "--- End next_results string ----"
    print
    print "--- kwargs string ----"
    kwargs = dict([ kv.split('=') for kv in next_results[1:].split("&") ])
    print kwargs
    print "--- End kwargs string ----"
    search_results = twitter_api.search.tweets(**kwargs)
    statuses += search_results['statuses']"""
    
    brand_name=brandToCheck
    brand_name=brand_name.lower()
    brand_name=brand_name.replace('#','')
    brand_name=brand_name.replace('.','')
    brand_name=brand_name.replace(' ','')
    brand_name=brand_name.replace('\'','')
#print str(len(statuses))
#print json.dumps(statuses[0].keys(), indent=1)
    """ Information about search_results:
    Has 2 keys , 'search_metadata' and 'statuses'
    search_results['statuses'] :- List 
    Each item in search_results['statuses'] is a dictionary with following keys:[u'contributors', u'truncated', u'text', u'in_reply_to_status_id', u'id', u'favorite_count', u'source', u'retweeted', u'coordinates', u'entities', u'in_reply_to_screen_name', u'in_reply_to_user_id', u'retweet_count', u'id_str', u'favorited', u'user', u'geo', u'in_reply_to_user_id_str', u'possibly_sensitive', u'lang', u'created_at', u'in_reply_to_status_id_str', u'place', u'metadata']
    """

#-------------------------------------------------------------------------- Insert into mongoDB ------------------------------------------------------


#--------------------------------------------------------------------------- Initialization ---------------------------------------------------------

    user_tweetdata=[]
    brand_tweetdata=[]
    
    
    retweet_count_brand=0
    tweet_count_usermention=0
    retweet_count_usermention=0
    tweet_count_keyword=0
    retweet_count_keyword=0
    tweet_count_hashtag=0
    retweet_count_hashtag=0
    goodtweet_count=0
    badtweet_count=0
    
    
    good_synonyms_set=['good','amazing','awesome','respectable','cool','happening','fantastic','wicked']
    bad_synonyms_set=['ugly','terrible','unsatisfied','bad','sucks','shit','sad','stupid']
    good_synonyms={}
    bad_synonyms={}

#---------------------------------------------------------------------------------------------------------------------------------------------------------

    status_texts = [ status['text'] 
                 for status in statuses ]
    screen_names = [ user_mention['screen_name'] 
                 for status in statuses
                     for user_mention in status['entities']['user_mentions'] ]
    counter = 0
    hashtags = [ hashtag['text'] 
             for status in statuses
                 for hashtag in status['entities']['hashtags'] ]


    words = [ w 
          for t in status_texts 
              for w in t.split() ]

	

    objectlist=[]    
    for tag in hashtags:
        if tag.lower() =="levis":
            counter = counter +1    
            objectlist.append(tag)



# Segregation of tweets into user tweets and brand tweets -----------------------------------------------------------


    for i in search_results['statuses']:
    	screen_name=i['user']['screen_name']
	screen_name=screen_name.lower()
	screen_name=screen_name.replace(',','')
	screen_name=screen_name.replace('#','')
	screen_name=screen_name.replace(' ','')
	screen_name=screen_name.replace('_','')
	if(screen_name.find('levis')!=-1):
		brand_tweetdata.append(i)
	else:
		user_tweetdata.append(i)

#----------------------------------------------------------------------------------------------------------------------



#------------------------------Re tweet count for brand tweets ------------------------------------------------------
    for i in brand_tweetdata:
    	retweet_count_brand += int(i['retweet_count'])

#----------------------------------------------------------------------------------------------------------------------

#-------------------------------------------------user tweeets with brand mentioon-------------------------------------
    for status in user_tweetdata:
    	if(brand_name in status['entities']['user_mentions']):
    		tweet_count_usermention += 1
		retweet_count_usermention += int(status['retweet_count'])

#----------------------------------------------------------------------------------------------------------------------

	

#-------------------------------------------------user tweeets with brand keyword-------------------------------------
    for status in user_tweetdata:
    	flag=0
    	word_text=status['text'].split(' ')
	for word in word_text:
		keyword=word.lower()
		keyword=keyword.replace(',','')
		keyword=keyword.replace('#','')
		keyword=keyword.replace('@','')
		keyword=keyword.replace('.','')
		keyword=keyword.replace('-','')
		keyword=keyword.replace(' ','')
		if(keyword.find(brand_name)!=-1):
			flag=1
	if(flag==1):
    		tweet_count_keyword += 1
		retweet_count_keyword += int(status['retweet_count'])

#----------------------------------------------------------------------------------------------------------------------



#-------------------------------------------------------- User tweets with brand Hashtag ------------------------------
    for status in user_tweetdata:
    	hashtag_brand='#'+brand_name
	if(status['text'].find(hashtag_brand)!=-1):
		tweet_count_hashtag += 1
		retweet_count_hashtag += int(status['retweet_count'])

#-------------------------------------------------------------------------------------------------------------------------
    	

    retweets = [
                # Store out a tuple of these three values ...
                (status['retweet_count'], 
                 status['retweeted_status']['user']['screen_name'],
                 status['text']) 
                
                # ... for each status ...
                for status in statuses 
                
                # ... so long as the status meets this condition.
                    if status.has_key('retweeted_status')
               ]

    # Slice off the first 5 from the sorted results and display each item in the tuple

    """pt = PrettyTable(field_names=['Count', 'Screen Name', 'Text'])
    [ pt.add_row(row) for row in sorted(retweets, reverse=True)[:5] ]
    pt.max_width['Text'] = 50
    pt.align= 'l'
    print pt"""
# --------------------------------------------------------------------------- Words in Tweets --------------------------------------------------------
    ct=Counter(words)
    ct_list=[]
    for i in ct:
	ct_list.append([i,ct[i]])

    for i in range(len(ct_list)):
	for j in range(len(ct_list)-1):
	    if(ct_list[i][1]>ct_list[j][1]):
		swap=ct_list[i]
		ct_list[i]=ct_list[j]
		ct_list[j]=swap
	
    output=[]
#brand_name='levis'
    count_brandword=0
    for i in range(len(ct_list)):
	d={}
	d['word']=ct_list[i][0]
	d['count']= ct_list[i][1]
	tweet_word=ct_list[i][0].lower()
	tweet_word=tweet_word.replace('#','')
	tweet_word=tweet_word.replace('\'','')
	tweet_word=tweet_word.replace('.','')
	if(tweet_word.find(brand_name)!=-1):
		count_brandword += int(ct_list[i][1])
	output.append(d)
    dic={}
    dic['brand_tweet']=count_brandword
    dic['total_tweet']=len(ct)
#---------------------------------------------------------------   Hash Tags  -----------------------------------------------------------------
    hash_count={}
    for i in hashtags:
    	if i in hash_count:
		hash_count[i] += 1
	else:
		hash_count[i] = 1
    ct_hashtags=Counter(hash_count)
    ct_hashlist=[]
    for i in ct_hashtags:
    	ct_hashlist.append([i,ct_hashtags[i]])

    for i in range(len(ct_hashlist)):
	    for j in range(len(ct_hashlist)-1):
		    if(ct_hashlist[i][1]>ct_hashlist[j][1]):
			    swap=ct_hashlist[i]
			    ct_hashlist[i]=ct_hashlist[j]
			    ct_hashlist[j]=swap
    output_hashtags=[]
    for i in range(len(ct_hashlist)):
    	dic_hash={}
	dic_hash['hash_tag']=ct_hashlist[i][0]
	dic_hash['count']=ct_hashlist[i][1]
	output_hashtags.append(dic_hash)


#------------------------------------------------------------ Sentiment module ----------------------------------------------------------
# respectable , cool , happening , fantastic , wicked
# bad, sucks , shit , stupid  , sad
    for i in good_synonyms_set:
    	good_synonyms[i]=[[i,5]]
    for i in bad_synonyms_set:
    	bad_synonyms[i]=[[i,-5]]


    for i in good_synonyms:	
	synsets = wordnet.synsets( i )
	for synset in synsets:
		for j in synset.lemma_names:
			j=j.replace("_"," ")
			if(j not in good_synonyms[i][0]):
				good_synonyms[i].append([j,5])
    for i in bad_synonyms:
	synsets = wordnet.synsets( i )
	for synset in synsets:
		for j in synset.lemma_names:
			j=j.replace("_"," ")
			if(j not in bad_synonyms[i][0]):
				bad_synonyms[i].append([j,-5])

	


    goodtweets=[]
    badtweets=[]
    for i in user_tweetdata:
    	heuristic_value=0
    	for j in good_synonyms:
		for k in good_synonyms[j]:
			if(i['text'].find(k[0])!=-1):
				heuristic_value += int(k[1])
	for j in bad_synonyms:
		for k in bad_synonyms[j]:
			if(i['text'].find(k[0])!=-1):
				heuristic_value += int(k[1])
	if(heuristic_value>=5):
		goodtweets.append(i['text'])
	elif(heuristic_value<=-5):
		badtweets.append(i['text'])



    goodtweet_count = len(goodtweets)
    badtweet_count = len(badtweets)



#-------------------------------------------------------------------------------- xxxxx --------------------------------------------------------------
		
#------------------------------------------------------------------------------ Print Section ------------------------------------------------------------
	


    for i in statuses[0]:
   	print i+ " : ", statuses[0][i]
	print

    for i in range(len(statuses)):
	    print 'Created at : ' + str(statuses[i]['user']['created_at'])
	    print 'Time zone : ' + str(statuses[i]['user']['time_zone'])
	

    print tweet_count_usermention
    print retweet_count_usermention	
    print tweet_count_keyword
    print retweet_count_keyword	
    print tweet_count_hashtag
    print retweet_count_hashtag	
    print 'Positive Tweets : ',goodtweet_count
    print 'Negative Tweets : ',badtweet_count
    print goodtweets
    print badtweets

#-------------------------------------------------------------------------------- xxxxx ----------------------------------------------------------------------

    return json.dumps({'data':output , 'count':dic , 'hashtags':output_hashtags,'brand_tweetcount':len(brand_tweetdata),'brand_retweetcount': retweet_count_brand,'user_tweetcount':len(user_tweetdata),'keyword_tweetcount':tweet_count_keyword,'keyword_retweetcount':retweet_count_keyword,'mention_tweetcount':tweet_count_usermention,'mention_retweetcount':retweet_count_usermention,'hashtag_tweetcount':tweet_count_hashtag,'hashtag_retweetcount':retweet_count_hashtag, 'goodtweets':goodtweets , 'badtweets':badtweets},indent=1)




@app.route('/socialdashboard', methods= ['GET','POST'])
def socialdashboard():
    return render_template('socialdashboard.html')












#Facebook login
#Use facepy later to post
'''
@app.route('/loginFB')
def login():
    return facebook.authorize(callback=url_for('facebook_authorized',
        next=request.args.get('next') or request.referrer or None,
        _external=True))


@app.route('/loginFB/authorized')
@facebook.authorized_handler
def facebook_authorized(resp):
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    session['oauth_token'] = (resp['access_token'], '')
    me = facebook.get('/me')
    #return 'Logged in as id=%s name=%s redirect=%s' % \
     #   (me.data['id'], me.data['name'], request.args.get('next'))
    return render_template('index_fblogin.html',
        title='hello',userobj=me
    )

@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('oauth_token')


#Facebook login end
'''

if __name__ == '__main__':
  app.run()
