from datetime import datetime
from flask_sqlalchemy import *
from myflaskapp import db
from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import text

class Banker(db.Model):
    __tablename__ = 'newton_banker'
    id = db.Column('banker_id', db.Integer, primary_key=True)
    banker_name = db.Column(db.String(100))
    banker_logo_url  = db.Column(db.String)
    banker_desc  = db.Column(db.String)

    def __init__(self, bankName, url, desc):
        self.banker_name = bankName
        self.banker_logo_url = url
        self.banker_desc = desc        

class Asset(db.Model):
    __tablename__ = 'newton_asset'
    id = db.Column('asset_id', db.Integer, primary_key=True)
    asset_url   = db.Column(db.String)
    asset_desc  = db.Column(db.String)
    asset_type  = db.Column(db.String(2))
    property_id = db.Column('property_id',db.Integer,db.ForeignKey('newton_property.property_id'))


    def __init__(self,assetUrl,assetDesc,assetType, property_id):
        self.asset_url = assetUrl
        self.asset_desc = assetDesc
        self.asset_type = assetType
        self.property_id = property_id

class Property(db.Model):
    __tablename__ = 'newton_property'
    id = db.Column('property_id', db.Integer, primary_key=True)
    property_nm = db.Column(db.String(64))
    property_desc = db.Column(db.String(255))
    property_building_progress = db.Column(db.String(3))
    property_funding_progress = db.Column(db.String(3))
    builder_id = db.Column(db.Integer)
    create_dttm = db.Column(db.DateTime)
    start_dttm = db.Column(db.DateTime)
    complete_dttm = db.Column(db.DateTime)
    funding_start_dttm = db.Column(db.DateTime)
    funding_complete_dttm = db.Column(db.DateTime)
    returns = db.Column(db.String(10))
    fee = db.Column(db.String(3))
    approval_status = db.Column(db.String(3))
    property_url_snapshot = db.Column(db.String(255))
    assets = db.relationship('Asset', backref='newton_property', lazy='select')

    def __init__(self,name):
        self.property_nm = name
        self.property_desc = "Some Desc"
        self.property_building_progress = "80"
        self.property_funding_progress = "90"
        self.builder_id = '123'
        self.create_dttm = datetime.utcnow();
        self.start_dttm = datetime.utcnow();
        self.complete_dttm = datetime.utcnow();
        self.funding_start_dttm = datetime.utcnow();
        self.funding_complete_dttm = datetime.utcnow();
        self.returns = "5%"
        self.fee = "3%"
        self.approval_status = "A"
        self.property_url_snapshot = "http://lorempixel.com/465/475/city/"


class User(db.Model):
    __tablename__ = 'newton_user'
    id = db.Column('user_id', db.Integer, primary_key=True)
    last_nm = db.Column(db.String(64))
    first_nm  = db.Column(db.String(64))
    middle_nm  = db.Column(db.String(64))
    primary_no = db.Column(db.String(64))
    email = db.Column(db.String(100))
    facebook_uname = db.Column(db.String(100))
    linkedin_uname = db.Column(db.String(100))
    create_dttm = db.Column(db.DateTime)
    update_dttm = db.Column(db.DateTime)
    modhash = db.Column(db.String(64))
    active = db.Column(db.Boolean)    
    password = db.Column(db.String(128))
    salt = db.Column(db.String(128))
    credit = db.Column(db.String(10))
    address = db.Column(db.String)
    user_flag = db.Column(db.String(1)) 
    registered_name = db.Column(db.String)
    registration_number = db.Column(db.String(100))
   

    def __init__(self,last_nm, first_nm, middle_nm, primary_no, \
        email,password, address,user_flag, registered_name, registration_number):
        self.last_nm = last_nm
        self.first_nm = first_nm
        self.middle_nm = middle_nm
        self.primary_no = primary_no
        self.email = email
        self.facebook_uname = "asda"
        self.linkedin_uname = "bsda"
        self.create_dttm = datetime.utcnow();
        self.update_dttm = datetime.utcnow();
        self.modhash = "abcdefghij"
        self.active = True
        self.password = password
        self.salt = "abcdefgh"
        self.credit="10000"
        self.address= address
        self.user_flag = user_flag
        self.registered_name = registered_name
        self.registration_number= registration_number
    

class UserData(db.Model):
    __tablename__ = 'userdata'
    id = db.Column('user_i_id',db.Integer, primary_key=True)
    user_id = db.Column(db.String(100))
    user_device= db.Column(db.String(100))
    user_age= db.Column(db.String(100))
    user_gender = db.Column(db.String)
    user_income = db.Column(db.String)
    user_occupation = db.Column(db.String)
    user_race= db.Column(db.String)
    user_date = db.Column(db.DateTime)
  
    def __init__(self,user_id,user_device,user_gender,user_income,user_occupation, \
        user_race,user_date):
        self.user_id = user_id
        self.user_device  = user_device
        self.user_age = user_age
        self.user_gender = user_gender
        self.user_income =  user_income
        self.user_occupation = user_occupation
        self.user_race = user_race
        self.user_date = user_date

class Segment(db.Model):
    __tablename__ = 'segments'
    id = db.Column('segment_id ',db.Integer,primary_key=True)
    segment_name  = db.Column(db.String(100))
    segment_age   = db.Column(db.String(100))
    segment_occupation = db.Column(db.String) 
    segment_gender   = db.Column(db.String)
    segment_income   = db.Column(db.String)
    segment_race     = db.Column(db.String)

    def __init__(self,segment_name,segment_age,segment_occupation,segment_gender,segment_income, \
        segment_race):
        self.segment_name = segment_name
        self.segment_age = segment_age
        self.occupation = segment_occupation
        self.segment_gender = segment_gender
        self.segment_income = segment_income
        self.segment_race = segment_race

# class Campaign(db.Model):
#     __tablename__  = 'campaigns'
#     id = db.Column('campaigns_id',db.Integer,primary_key = True)
#     campaign_name = db.Column(db.String)
#     campaign_banner = db.Column(db.String(50))
#     campaign_mobile = db.Column(db.String(255))
#     campaign_sms = db.Column(db.String(255))
   
#     def __init__(self,campaign_name,campaign_banner,campaign_mobile, \
#         campaign_sms):
#         self.campaign_name = campaign_name
#         self.campaign_banner = campaign_banner
#         self.campaign_mobile = campaign_mobile
#         self.campaign_sms = campaign_sms

class UserTraffic(db.Model):
    __tablename__ = 'usertraffic'
    id = db.Column('traffic_id ',db.Integer,primary_key = True)
    user_i_id  = db.Column(db.Integer)
    user_id    = db.Column(db.Integer)   
    visit_at   = db.Column(db.DateTime)

    def __init__(self,user_i_id,user_id,visit_at):
       self.user_i_id = user_i_id
       self.user_id = user_id
       self.visit_at = visit_at


class CampaignData(db.Model):
    __tablename__  = 'campaign'
    id = db.Column('campaign_id',db.Integer,primary_key = True)
    campaign_name = db.Column(db.String)
    campaign_assertid = db.Column(db.Integer)
    assetscamps = db.relationship('CampaignAssert', backref='campaign', lazy='select')
    
    def __init__(self,campaign_name,campaign_assertid):
        self.campaign_name = campaign_name
        self.campaign_assertid = campaign_assertid
        


class CampaignAssert(db.Model):
    __tablename__ = 'campaign_asserts'
    id = db.Column('asserts_id',db.Integer,primary_key = True)
    campaign_id = db.Column('camp_id',db.Integer,db.ForeignKey('campaign.campaign_id'))
    assert_type = db.Column(db.String(255))
    asserts_clicks = db.Column(db.Integer)
    asserts_views = db.Column(db.Integer)
    asserts_ratio  = db.Column(db.Integer)
    
    def __init__(self,assert_type,asserts_clicks,asserts_views,asserts_ratio,campaign_id):
        self.assert_type = assert_type
        self.asserts_clicks = asserts_clicks
        self.asserts_views = asserts_views
        self.asserts_ratio = campaign_endDate
        self.campaign_id = campaign_id





class Campaigns(db.Model):
    __tablename__ = 'campaigns'
    id = db.Column('campaign_id', db.Integer, primary_key=True)
    campaign_name = db.Column(db.String(64))
    start_date = db.Column(db.DateTime)
    end_date= db.Column(db.DateTime)
    budget = db.Column(db.Integer)
    frequency_cap = db.Column(db.Integer)
    gender = db.Column(db.String(20))
    income = db.Column(db.String(20))
    agegroup = db.Column(db.String(20))
    occupation = db.Column(db.String(20))
    race = db.Column(db.String(20))
    recency = db.Column(db.String(30))
    frequency = db.Column(db.String(30))
    monthly_income = db.Column(db.String(30))
    target_visitor = db.Column(db.String(30))
    distance_range = db.Column(db.String(30))
    mobile_sms = db.Column(db.String(255))
    banner = db.Column(db.String(255))
    video = db.Column(db.String(255))
  
    def __init__(self, campaign_name, start_date, end_date, budget, \
        frequency_cap,gender,income,agegroup,occupation,race,recency, \
        frequency,monthly_income,target_visitor,distance_range,mobile_sms,banner,video):
        self.campaign_name = campaign_name
        self.start_date = start_date
        self.end_date = end_date
        self.budget = budget
        self.frequency_cap = frequency_cap
        self.gender = gender
        self.income = income
        self.agegroup = agegroup
        self.occupation = occupation
        self.race = race
        self.recency = recency
        self.frequency = frequency
        self.monthly_income = monthly_income
        self.target_visitor = target_visitor
        self.distance_range = distance_range
        self.mobile_sms = mobile_sms
        self.banner = banner
        self.video = video


class Store(db.Model):
    __tablename__ = 'store'
    id = db.Column('storekey_id', db.Integer, primary_key=True)
    store_id=db.Column(db.Integer)
    store_date =db.Column(db.DateTime)
    store_hour = db.Column(db.Integer)
    store_min = db.Column(db.Integer)
    store_gender = db.Column(db.String(1))
    store_age = db.Column(db.String(20))
    store_dwelltime = db.Column(db.String(20))
    def __init__(self,storeId,hour,min,gender,age,dwelltime):
        self.store_id = storeId
        self.store_date = date
        self.store_hour = hour
        self.store_min = min
        self.store_gender = gender
        self.store_age = age
	self.store_dwelltime = dwelltime


class Proof(db.Model):
	__tablename__ = 'proof'
	id = db.Column('proof_id' , db.Integer , primary_key=True)
	proof_date = db.Column(db.DateTime)
	proof_hour = db.Column(db.Integer)
	proof_min = db.Column(db.String(2))
	proof_package = db.Column(db.String(20))
	proof_station = db.Column(db.String(20))
	proof_content = db.Column(db.String(20))
	proof_count = db.Column(db.Integer)
	proof_duration = db.Column(db.String(20))
	
	def __init__(self,hour,min,package,station,content,count,duration):
		self.proof_hour = hour
		self.proof_min = min
		self.proof_package = package
		self.proof_station = station
		self.proof_content = content
		self.proof_count = count
		self.proof_count = duration
