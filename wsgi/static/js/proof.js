$(document).ready(function() {		
	var url="http://127.0.0.1:5000";

	
	
	$("#proof123").on("click",function(){
		
		var allstore = $("#all-store").val();
		var selectId1 = $("#Store1").val();
		var selectId2 = $("#Store2").val();
		var selectId3 = $("#Store3").val();
		var selectId4 = $("#Store4").val();
		var selectId5 = $("#Store5").val();
		var lastDay = $("#lastday").val();
		var lastWeek = $("#lastweek").val();
		var selectDate = $("#selectdate").val();	
		var sDate = $("#start_date").val();
		var eDate = $("#end_date").val();	
		var currentDate = new Date();
		var storeId="",startDate="",endDate= "";
		var day_flag=0;
		var diff_days=0;
		
		if($('#all-store').is(':checked'))
		{
			
			storeId="all";
		}
		else
		{
			storeId= selectId1 + "," + selectId2 + "," + selectId3 + "," + selectId4 + "," + selectId5;
		}
		
		if($('#lastday').is(':checked'))
		{
			var startDate1 = new Date(currentDate.setDate(currentDate.getDate() - 1));
			startDate=parse_date(startDate1);
			endDate = startDate;
			day_flag=1;
			diff_days=0;
			
		}
		else if($('#lastweek').is(':checked'))
		{
			
			var startDate1 = new Date(currentDate.setDate(currentDate.getDate() - 7));
			startDate = parse_date(startDate1);
			var endDate1 = new Date(currentDate.setDate(currentDate.getDate() + 6));
			endDate = parse_date(endDate1);
			diff_days=7;
		}
		else
		{
			startDate=sDate;
			endDate=eDate;
			if(startDate === endDate)
			{
				day_flag=1
			}
		}
		//console.log("-------------");
		//console.log(storeId);
		//console.log(startDate);
		//console.log(endDate);
		//console.log("-------------");
		getStore(url+"/proofdata",storeId,startDate,endDate,day_flag,diff_days,"chart1","chart2");

    	});

});




function Find_date(currentDate,nunber)
{
	return currentDate.setDate(currentDate.getDate() - number);
	
}
function parse_date(currentDate)
{
		var day = currentDate.getDate() ;
		var month =currentDate.getMonth() + 1 ;
		var year = currentDate.getFullYear()
		if(Number(month) < 10)
			month = "0" + month;
		if(Number(day) < 10)
			day = "0" + month;
		return year + "-" + month + "-" + day;
}


function BrandVStore(data,tag,brandname)
{
	var chartItems = new Array();
	bs_data=data.data_store.brand_data;
	var first_row=new Array();
	first_row.push('Store-ID')
	for(key in bs_data)
	{
		for(brand in bs_data[key])
		{
			first_row.push(brand.toString());
		}
		break;
	}
	chartItems.push(first_row);
	for(key in bs_data)
	{
		var row=new Array();
		row.push('ID '+key.toString());
		for(brand in bs_data[key])
		{
			row.push(bs_data[key][brand]);
		}
		chartItems.push(row);
	}
	google.load("visualization", "1", {packages:["corechart"]}); 	
        var data = google.visualization.arrayToDataTable(chartItems);

        var options = {
          title: brandname,
          hAxis: {title: 'Year', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById(tag));
        chart.draw(data, options);
      

}

function BrandVDay(data,tag,startDate,endDate,brandname)
{
	string=startDate.split('-');
	s=new Date(Number(string[0]),Number(string[1])-1,Number(string[2]));
	string=endDate.split('-');
	e=new Date(Number(string[0]),Number(string[1])-1,Number(string[2]));
	var chartItems = new Array();
	var bt_data=data.data_time;
	var first_row = new Array();
	first_row.push('Day');
	for (brand in bt_data)
	{
		first_row.push(brand);
	}
	chartItems.push(first_row);
	for (var d = s; d <= e; d.setDate(d.getDate() + 1))
	{	
		var day_string=parse_date(d);
		
		var row= new Array();
		row.push(day_string);
		for(key in bt_data)
		{
			var flag=0;
			list=Object.keys(bt_data[key]['graph_data']);
			for(var i=0;i<list.length;i++)
			{
				if(day_string === list[i])
				{
					flag=1;
				}
			}
			if(flag === 1)
			{
					row.push(bt_data[key]['graph_data'][day_string]);
			}
			else
			{
				row.push(0);
			}
			
		}	
		chartItems.push(row);
	}

        google.load("visualization", "1", {packages:["corechart"]});
        var data = google.visualization.arrayToDataTable(chartItems);

        var options = {
          title:  brandname
        };

        var chart = new google.visualization.LineChart(document.getElementById(tag));
        chart.draw(data, options);

}
function BrandVHour(data,tag)
{
	var chartItems = new Array();
	var bt_data=data.data_time;
	var first_row = new Array();
	first_row.push('Day');
	for (brand in bt_data)
	{
		first_row.push(brand);
	}
	chartItems.push(first_row);

	for(var hour=0;hour<24;hour++)
	{	
		var row= new Array();
		row.push(hour.toString());
		for(key in bt_data)
		{
				row.push(bt_data[key]['graph_data'][hour.toString()]);
		}
		chartItems.push(row);
	}
	//console.log(chartItems);
        google.load("visualization", "1", {packages:["corechart"]});
        var data = google.visualization.arrayToDataTable(chartItems);

        var options = {
          title: 'XXXX'
	  

        };

        var chart = new google.visualization.LineChart(document.getElementById(tag));
        chart.draw(data, options);


}




function promotionVDay(data,tag,startDate,endDate,brand_name)
{
	string=startDate.split('-');
	s=new Date(Number(string[0]),Number(string[1])-1,Number(string[2]));
	string=endDate.split('-');
	e=new Date(Number(string[0]),Number(string[1])-1,Number(string[2]));
	var chartItems = new Array();
	var bt_data=data;
	var first_row = new Array();
	first_row.push('Day');
	for (brand in bt_data)
	{
		first_row.push(brand);
	}
	chartItems.push(first_row);
	for (var d = s; d <= e; d.setDate(d.getDate() + 1))
	{	
		var day_string=parse_date(d);
				
		var row= new Array();
		row.push(day_string);
		for(key in bt_data)
		{
			var flag=0;
			list=Object.keys(bt_data[key]);
			for(var i=0;i<list.length;i++)
			{
				if(day_string === list[i])
				{
					flag=1;
				}
			}
			if(flag === 1)
			{
					row.push(bt_data[key][day_string]);
			}
			else
			{
				row.push(0);
			}
			
		}	
		chartItems.push(row);
	}

        google.load("visualization", "1", {packages:["corechart"]});
        var data = google.visualization.arrayToDataTable(chartItems);

        var options = {
          title: brand_name
        };

        var chart = new google.visualization.LineChart(document.getElementById(tag));
        chart.draw(data, options);

}

function promotionVStore(data,tag,promotion_names)
{
    //console.log(tag);
    var chartItems = new Array();
    var first_row = new Array();
    first_row.push('Promotions');
    for(var i=0;i<promotion_names.length;i++)
    {
	first_row.push(promotion_names[i].toString());
    }
	chartItems.push(first_row);
    	for(station in data)
	{
		var row = new Array();
		row.push(station.toString());
		for(promotion in data[station])
		{
			var flag=0;
			for(var i=0;i<promotion_names.length;i++)
			{
				if(promotion == promotion_names[i])
					flag=1;
			}
			if(flag === 1)
			{
				//console.log("true");
				row.push(Number(data[station][promotion]));
			}
			else
			{
				//console.log("false");
				row.push(0);
			}
		}
		chartItems.push(row);
	}
	//console.log(chartItems);

    google.load("visualization", "1", {packages:["corechart"]});

      var data = google.visualization.arrayToDataTable(chartItems);


      var options = {
        width: 600,
        height: 400,
        legend: { position: 'top', maxLines: 3 },
	bar: { groupWidth: '75%' },
        isStacked: true,
      };
      var chart = new google.visualization.ColumnChart(document.getElementById(tag));
      chart.draw(data, options);
  

}

function promotionVHour(data,tag,brand_name)
{
	var chartItems = new Array();
	var bt_data=data;
	var first_row = new Array();
	first_row.push('Hour');
	for (brand in bt_data)
	{
		first_row.push(brand);
	}
	chartItems.push(first_row);
	
	for(var hour=0;hour<24;hour++)
	{	
		var row= new Array();
		row.push(hour.toString());
		for(key in bt_data)
		{
			
				row.push(bt_data[key][hour.toString()]);
		
		}
		chartItems.push(row);
	}	
	//console.log(chartItems);
	//console.log(tag);


	google.load("visualization", "1", {packages:["corechart"]});
        var data = google.visualization.arrayToDataTable(chartItems);

        var options = {
          title: brand_name
        };

        var chart = new google.visualization.LineChart(document.getElementById(tag));
        chart.draw(data, options);


}











function getStore(url,storeId,startDate,endDate,day_flag,diff_days,Gendertag,Agetag)
{

	//console.log(day_flag);
	var myKeyVals = { 'storeId' : storeId , 'startDate': startDate , 'endDate': endDate , 'day_flag': day_flag,'diff_days': diff_days}
	
    	$.ajax({
		type:"POST",
		url:url,
		data:JSON.stringify(myKeyVals),
		dataType:"json",
		contentType:"application/json; charset=utf-8",
		success:function(data)
		{
			console.log(data);
			console.log("yaay ! success :) !");
			BrandVStore(data,"chart1");
			if(day_flag==0)
			{
				BrandVDay(data,"chart2",startDate,endDate);
			}
			else
			{
				BrandVHour(data,"chart2");
			}
//----------------------------------------- Plotting promotion data of each brand with respect to Store ID -----------------------------------------------------------------------


			brand_names = data.data_time;
			var tag_string="chart";
			var count=3;
			for(brand_name in brand_names)
			{
				var tag=tag_string + count.toString();
				var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+tag+"\"></div></div>"
				var div_tag="#chart"+count.toString();
				$(div_tag).html(html);
				promotionVStore(brand_names[brand_name]['store_data'],tag,brand_names[brand_name]['promotion_names'],brand_name);
				count += 1;
			}
			

			
	

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/* -------------------------------------    Plotting promotion data of each brand with respect to Time ---------------------------------------------------------------------------*/
			
			
			if(day_flag==0)
			{
				for(brand_name in brand_names)
				{
					
				var tag = tag_string + count.toString();
					var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+tag+"\"></div></div>"
				var div_tag="#chart"+count.toString();
				$(div_tag).html(html);
				promotionVDay(brand_names[brand_name]['promotion_data'],tag,startDate,endDate,brand_name);
					count += 1;
				}
			}
			else
			{
				for(brand_name in brand_names)
				{
					var tag = tag_string + count.toString();
					var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+tag+"\"></div></div>"
				var div_tag="#chart"+count.toString();
				$(div_tag).html(html);
					promotionVHour(brand_names[brand_name]['promotion_data'],tag,brand_name);
					count += 1;
				}				
			}			
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





		},
		error: function(msg)
		{ 
		   
		    //console.log("beep!");
		}

    	});

}


