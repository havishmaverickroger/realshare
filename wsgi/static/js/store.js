$(document).ready(function() {		
	var url="http://127.0.0.1:5000";

	
	
	$("#store123").on("click",function(){
		
		var allstore = $("#all-store").val();
		var selectId1 = $("#Store1").val();
		var selectId2 = $("#Store2").val();
		var selectId3 = $("#Store3").val();
		var selectId4 = $("#Store4").val();
		var selectId5 = $("#Store5").val();
		var lastDay = $("#lastday").val();
		var lastWeek = $("#lastweek").val();
		var selectDate = $("#selectdate").val();	
		var sDate = $("#start_date").val();
		var eDate = $("#end_date").val();	
		var currentDate = new Date();
		var storeId="",startDate="",endDate= "";
		var day_flag=0;
		var diff_days=0;
		
		if($('#all-store').is(':checked'))
		{
			
			storeId="all";
		}
		else
		{
			storeId= selectId1 + "," + selectId2 + "," + selectId3 + "," + selectId4 + "," + selectId5;
		}
		
		if($('#lastday').is(':checked'))
		{
			var startDate1 = new Date(currentDate.setDate(currentDate.getDate() - 1));
			startDate=parse_date(startDate1);
			endDate = startDate;
			day_flag=1;
			diff_days=0;
			
		}
		else if($('#lastweek').is(':checked'))
		{
			
			var startDate1 = new Date(currentDate.setDate(currentDate.getDate() - 7));
			startDate = parse_date(startDate1);
			var endDate1 = new Date(currentDate.setDate(currentDate.getDate() + 6));
			endDate = parse_date(endDate1);
			diff_days=7;
		}
		else
		{
			startDate=sDate;
			endDate=eDate;
			if(startDate === endDate)
			{
				day_flag=1
			}
		}
		console.log("-------------");
		console.log(storeId);
		console.log(startDate);
		console.log(endDate);
		console.log("-------------");
		getStore(url+"/storedata",storeId,startDate,endDate,day_flag,diff_days,"chart1","chart2");

    	});

});
function GenderChart(data,tag)
{
	var chartItems = new Array();
		google.load("visualization", "1", {packages:["corechart"]});

			chartItems.push(['Store ID','Male','Female',{ role: 'annotation'}]);
			var mf_data= data.data;
			for(var key in mf_data)
			{
				chartItems.push(['ID '+key,mf_data[key]['male_count'],mf_data[key]['female_count'],'']);
			}
			var data = google.visualization.arrayToDataTable(chartItems);
			
			


		 	var options = {
			width: 600,
			height: 400,
			legend: { position: 'top', maxLines: 3 },
			bar: { groupWidth: '75%' },
			isStacked: true,
		      };
	
		      var chart = new google.visualization.ColumnChart(document.getElementById(tag));
		     chart.draw(data, options);
}

function AgeChart(data,tag)
{
		var chartItems = new Array();
		google.load("visualization", "1", {packages:["corechart"]});

			chartItems.push(['Store ID','junior','young-adult','adult','senior',{ role: 'annotation'}]);
			var mf_data= data.data;
			for(var key in mf_data)
			{
				chartItems.push(['ID '+key,mf_data[key]['junior'],mf_data[key]['young-adult'],mf_data[key]['adult'],mf_data[key]['senior'],'']);
			}
			var data = google.visualization.arrayToDataTable(chartItems);
		 	var options = {
			width: 600,
			height: 400,
			legend: { position: 'top', maxLines: 3 },
			bar: { groupWidth: '75%' },
			isStacked: true,
		      };
	
		      var chart = new google.visualization.ColumnChart(document.getElementById(tag));
		     chart.draw(data, options);
}
function DayChart(type,data,tag,startDate,endDate)
{
	string=startDate.split('-');
	s=new Date(Number(string[0]),Number(string[1])-1,Number(string[2]));
	string=endDate.split('-');
	e=new Date(Number(string[0]),Number(string[1])-1,Number(string[2]));
	var chartItems = new Array();
	google.load("visualization", "1", {packages:["corechart"]});
	day_data = data.data;
	var first_row=new Array();
	first_row.push('Day');
	for ( key in day_data)
	{
		first_row.push('ID '+key);
	}
	chartItems.push(first_row);
	for (var d = s; d <= e; d.setDate(d.getDate() + 1))
	{	
		var day_string=parse_date(d);
		console.log(day_string);		
		var row= new Array();
		row.push(day_string);
		for(key in day_data)
		{
			var flag=0;
			list=Object.keys(day_data[key]['graph_data']);
			for(var i=0;i<list.length;i++)
			{
				if(day_string === list[i])
				{
					flag=1;
				}
			}
			if(flag === 1)
			{
				if(type=="Total")
					row.push(day_data[key]['graph_data'][day_string]['male_count']+day_data[key]['graph_data'][day_string]['female_count']);
				else if(type=="Male")
					row.push(day_data[key]['graph_data'][day_string]['male_count']);
				else if(type=="Female")
					row.push(day_data[key]['graph_data'][day_string]['female_count']);
				else if(type=="Junior")
					row.push(day_data[key]['graph_data'][day_string]['junior']);
				else if(type=="YoungAdult")
					row.push(day_data[key]['graph_data'][day_string]['young-adult']);
				else if(type=="Adult")
					row.push(day_data[key]['graph_data'][day_string]['adult']);
				else if(type=="Senior")
					row.push(day_data[key]['graph_data'][day_string]['senior']);
			}
			else
			{
				row.push(0);
			}
			
		}	
		chartItems.push(row);
	}	
        var data = google.visualization.arrayToDataTable(chartItems);

        var options = {
          title: 'Company Performance'
        };

        var chart = new google.visualization.LineChart(document.getElementById(tag));
        chart.draw(data, options);

}
function HourChart(type,data,tag)
{
	var chartItems = new Array();
	google.load("visualization", "1", {packages:["corechart"]});
	hour_data = data.data;
	var first_row=new Array();
	first_row.push('Hour');
	for ( key in hour_data)
	{
		first_row.push('ID '+key);
	}
	chartItems.push(first_row);
	
	for(var hour=0;hour<24;hour++)
	{	
		var row= new Array();
		row.push(hour.toString());
		for(key in hour_data)
		{
			if(type=="Total")
				row.push(hour_data[key]['graph_data'][hour.toString()]['male_count']+hour_data[key]['graph_data'][hour.toString()]['female_count']);
			else if(type=="Male")
				row.push(hour_data[key]['graph_data'][hour.toString()]['male_count']);
			else if(type=="Female")
				row.push(hour_data[key]['graph_data'][hour.toString()]['female_count']);
			else if(type=="Junior")
				row.push(hour_data[key]['graph_data'][hour.toString()]['junior']);
			else if(type=="YoungAdult")
				row.push(hour_data[key]['graph_data'][hour.toString()]['young-adult']);
			else if(type=="Adult")
				row.push(hour_data[key]['graph_data'][hour.toString()]['adult']);
			else if(type=="Senior")
				row.push(hour_data[key]['graph_data'][hour.toString()]['senior']);

		}
		chartItems.push(row);
	}	
        var data = google.visualization.arrayToDataTable(chartItems);

        var options = {
          title: 'Company Performance'
        };

        var chart = new google.visualization.LineChart(document.getElementById(tag));
        chart.draw(data, options);
}


function Find_date(currentDate,nunber)
{
	return currentDate.setDate(currentDate.getDate() - number);
	
}
function parse_date(currentDate)
{
		var day = currentDate.getDate() ;
		var month =currentDate.getMonth() + 1 ;
		var year = currentDate.getFullYear()
		if(Number(month) < 10)
			month = "0" + month;
		if(Number(day) < 10)
			day = "0" + month;
		return year + "-" + month + "-" + day;
}


function getStore(url,storeId,startDate,endDate,day_flag,diff_days,Gendertag,Agetag)
{

	console.log(day_flag);
	var myKeyVals = { 'storeId' : storeId , 'startDate': startDate , 'endDate': endDate , 'day_flag': day_flag,'diff_days': diff_days}
	
    	$.ajax({
		type:"POST",
		url:url,
		data:JSON.stringify(myKeyVals),
		dataType:"json",
		contentType:"application/json; charset=utf-8",
		success:function(data){
			GenderChart(data,Gendertag);
			AgeChart(data,Agetag);
			if(day_flag==1)
			{


		
var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph3"+"\"></div></div>"
				$("#chart3").html(html);
				HourChart("Total",data,"graph3");

			
var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph4"+"\"></div></div>"
				$("#chart4").html(html);
				HourChart("Male",data,"graph4");
var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph5"+"\"></div></div>"
				$("#chart5").html(html);
				HourChart("Female",data,"graph5");

var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph6"+"\"></div></div>"
				$("#chart6").html(html);
				HourChart("Junior",data,"graph6");


var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph7"+"\"></div></div>"
				$("#chart7").html(html);
				HourChart("YoungAdult",data,"graph7");

var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph8"+"\"></div></div>"
				$("#chart8").html(html);
				HourChart("Adult",data,"graph8");



var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph9"+"\"></div></div>"
				$("#chart9").html(html);
				HourChart("Senior",data,"graph9");

			}
			else
			{


				
var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph3"+"\"></div></div>"
				$("#chart3").html(html);
				DayChart("Total",data,"graph3",startDate,endDate);

var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph4"+"\"></div></div>"
				$("#chart4").html(html);
				DayChart("Male",data,"graph4",startDate,endDate);

var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph5"+"\"></div></div>"
				$("#chart5").html(html);
				DayChart("Female",data,"graph5",startDate,endDate);

var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph6"+"\"></div></div>"
				$("#chart6").html(html);
				DayChart("Junior",data,"graph6",startDate,endDate);


var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph7"+"\"></div></div>"
				$("#chart7").html(html);
				DayChart("YoungAdult",data,"graph7",startDate,endDate);


var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph8"+"\"></div></div>"
				$("#chart8").html(html);
				DayChart("Adult",data,"graph8",startDate,endDate);

var html = "<div class=\"panel panel-default\"><div class=\"panel-heading\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Brand Promotions Duration Vs Store ID</div ><div id=\""+"graph9"+"\"></div></div>"
				$("#chart9").html(html);
				DayChart("Senior",data,"graph9",startDate,endDate);
			}


		},
		error: function(msg)
		{ 
		   
		    console.log("beep!");
		}

    	});

}


