
$(document).ready(function() {		

    urlString = "http://127.0.0.1:5000";

    $("#compare").on("click",function(){;
	var brand1toCompare = $("#brand1").val();
	var brand2toCompare = $("#brand2").val();
	getTwitter(urlString+"/twitterpage","tweetcount1",brand1toCompare,"pie1","#tag1","#tweet1","#tweet1_brand","#tweet1_user","#tweettry");
	getTwitter(urlString+"/twitterpage","tweetcount2",brand2toCompare,"pie2","#tag2","#tweet2","#tweet2_brand","#tweet2_user","#tweettry2");
	/*drawFacebookCharts(urlString+"/facebookpage",'fbLikesByLifetimeBrand1',brand1toCompare,'fans_lifetime');
	  drawFacebookCharts(urlString+"/facebookpage",'fbLikesByLifetimeBrand2',brand2toCompare,'fans_lifetime');

	  drawFacebookCharts(urlString+"/facebookpage",'fbTalkingByDayBrandDaily1',brand1toCompare,'talking_day');
	  drawFacebookCharts(urlString+"/facebookpage",'fbTalkingByDayBrandDaily2',brand2toCompare,'talking_day');

	  drawFacebookCharts(urlString+"/facebookpage",'fbTalkingByDayBrandWeekly1',brand1toCompare,'talking_week');
	  drawFacebookCharts(urlString+"/facebookpage",'fbTalkingByDayBrandWeekly2',brand2toCompare,'talking_week');

	  drawFacebookCharts(urlString+"/facebookpage",'fbTalkingByDayBrandMonthly1',brand1toCompare,'talking_month');
	  drawFacebookCharts(urlString+"/facebookpage",'fbTalkingByDayBrandMonthly2',brand2toCompare,'talking_month');*/
    });

	function ajaxindicatorstart(text)
	{
		if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
		jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="ajax-loader.gif"><div><big>'+text+'</big></div></div><div class="bg"></div></div>');
		}

		jQuery('#resultLoading').css({
			'width':'100%',
			'height':'100%',
			'position':'fixed',
			'z-index':'10000000',
			'top':'0',
			'left':'0',
			'right':'0',
			'bottom':'0',
			'margin':'auto'
		});

		jQuery('#resultLoading .bg').css({
			'background':'#000000',
			'opacity':'0.7',
			'width':'100%',
			'height':'100%',
			'position':'absolute',
			'top':'0'
		});

		jQuery('#resultLoading>div:first').css({
			'width': '250px',
			'height':'75px',
			'text-align': 'center',
			'position': 'fixed',
			'top':'0',
			'left':'0',
			'right':'0',
			'bottom':'0',
			'margin':'auto',
			'font-size':'16px',
			'z-index':'10',
			'color':'#ffffff'

		});

	    jQuery('#resultLoading .bg').height('100%');
	       jQuery('#resultLoading').fadeIn(300);
	    jQuery('body').css('cursor', 'wait');
	}

	function ajaxindicatorstop()
	{
	    jQuery('#resultLoading .bg').height('100%');
	       jQuery('#resultLoading').fadeOut(300);
	    jQuery('body').css('cursor', 'default');
	}

	jQuery(document).ajaxStart(function () {
	 		//show ajax indicator
	ajaxindicatorstart('Loading Data.. please wait..');
	}).ajaxStop(function () {
	//hide ajax indicator
	ajaxindicatorstop();
	});
});


function toCap(someword)
{
    str = someword;
    str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
	return letter.toUpperCase();
    });
    return str;
}

function WordCount(data1,brandName,chartToDraw)
{
	    var chartItems = new Array();
	    var out=data1.data;
	    chartItems.push(["word","count",{role: "style"}]);              
	    for(i=0;i<10;i++)
	    {
		chartItems.push([out[i].word,Number(out[i].count),"gold"]);
	    }         

	    var data = google.visualization.arrayToDataTable(chartItems);

	    var view = new google.visualization.DataView(data);
	    view.setColumns([0, 1,
		    { calc: "stringify",
			sourceColumn: 1,
		type: "string",
		role: "annotation" },
		2]);

	    var options = {
		title: "Word Count in Tweets for " + brandName,
		width: 400,
		height: 200,
		bar: {groupWidth: "95%"},
		legend: { position: "none" },
	    };
	    var chart = new google.visualization.ColumnChart(document.getElementById(chartToDraw));
	    chart.draw(view, options);
}
function Tweets(data1,brand_tweetid,user_tweetid)
{
		var brand_tweetdata="<div class=\"rectangle\" id=\"one\">Total Tweets by Brand : " + data1.brand_tweetcount+ "</div><div class=\"rectangle\" id=\"two\">Total Retweets : " + data1.brand_retweetcount + "</div>";
		$(brand_tweetid).html(brand_tweetdata);
		var user_tweetdata="<div class=\"rectangle\" id=\"one\">Tweets with Brand keyword : " +data1.keyword_tweetcount + "</div><div class=\"rectangle\" id=\"two\">Corresponding Retweets : "+ data1.keyword_retweetcount +"</div><hr /><div class=\"rectangle\" id=\"one\">Tweets with Brand Mentions: " + data1.mention_tweetcount +"</div><div class=\"rectangle\" id=\"two\">Corresponding Retweets : " + data1.mention_retweetcount +"</div><hr /><div class=\"rectangle\" id=\"one\">Tweets with Brand Hashtags : " + data1.hashtag_tweetcount +"</div><div class=\"rectangle\" id=\"two\">Corresponding Retweets : " + data1.hashtag_retweetcount+ "</div><hr/>";
		$(user_tweetid).html(user_tweetdata);
	    //------------------------------------------
}
function Hash(data1,hashtag_id)
{
		    var hash_data=data1.hashtags;
		    var hash_html="<div> <style>.button {appearance: button;-moz-appearance: button;-webkit-appearance: button;text-decoration: none; font: menu;    color: Blue;display: inline-block;padding: 2px 8px;}</style> <div class=\"container\"> ";
		    for(i=0;i<10 && i<hash_data.length;i++)
		    {if(i%3===0)
			{ 
			    hash_html += "<br/>"
			}

			hash_html +="<a  class=\"button\"><i class=\"icon-tags\"></i>&nbsp&nbsp&nbsp"+hash_data[i]['hash_tag']+"</a>"
		    }
		    hash_html += "</div>";
		    hash_html += "</div>";
		    $(hashtag_id).html(hash_html);
}
function PieChart(data1,brandName,pietag)
{
	    var pieItems = new Array();
	    google.load("visualization", "1", {packages:["corechart"]});
	    pieItems.push(['sentiment','tweet Count']);
	    var good_tweet=data1.goodtweets;
	    var bad_tweet=data1.badtweets;
	    pieItems.push(['Positive sentiment tweets',good_tweet.length])
		pieItems.push(['Negative sentiment tweets',bad_tweet.length])
		var data = google.visualization.arrayToDataTable(pieItems);

	    var options = {
		title: brandName + ' Analysis'
	    };

	    var chart = new google.visualization.PieChart(document.getElementById(pietag));
	    chart.draw(data, options);
}
function goodbad(data1,tweet_id,tweettry)
{
		var goodtweets=data1.goodtweets;
		var ct;
		var temp_html = "<div id=\"navbarExample\" class=\"navbar navbar-static\"><div class=\"navbar-inner\"><div class=\"container\" style=\"width: auto;\"><a class=\"brand\" href=\"#\">Positive Sentiment Tweets</a><ul class=\"nav\"></ul></div></div></div><div data-spy=\"scroll\" data-target=\"#navbarExample\" data-offset=\"0\" class=\"scrollspy-example\">";

		for(var i=0;i< goodtweets.length && i<3;i++)
		{
		    ct=i+1;
			temp_html += "<h4> Tweet"+ct+"</h4><p>" + goodtweets[i] +"</p>";
		}
			temp_html += "</div>";
		$(tweet_id).html(temp_html);
		var badtweets=data1.badtweets;

		var temp_html1 = "<div id=\"navbarExample\" class=\"navbar navbar-static\"><div class=\"navbar-inner\"><div class=\"container\" style=\"width: auto;\"><a class=\"brand\" href=\"#\">Negative Sentiment Tweets</a><ul class=\"nav\"></ul></div></div></div><div data-spy=\"scroll\" data-target=\"#navbarExample\" data-offset=\"0\" class=\"scrollspy-example\">";

		for(var i=0;i< badtweets.length && i<3;i++)
		{
		    ct = i+1;
			temp_html1 += "<h4 id=\"fat\"> Tweet "+ct+"</h4><p>" + badtweets[i] +"</p>";
		}
			temp_html1 += "</div>";
		$(tweettry).html(temp_html1);

}
function getTwitter(url,chartToDraw,brandName,pietag,hashtag_id,tweet_id,brand_tweetid,user_tweetid,tweettry)
{
	    var chartItems = new Array();
	    var pieItems = new Array();
	    var dataItems = new Array();
	    var myKeyVals = { 'brandname' : brandName}
	    $.ajax({
		type:"POST",
		url:url,
		data:JSON.stringify(myKeyVals),
		dataType:"json",
		contentType:"application/json; charset=utf-8",
		success:function(data1){
	    //--------------------------------------------------------------------------------Word Count --------------------------------------------------------------------------------
		    WordCount(data1,brandName,chartToDraw);
	    //---------------------------------------------------------------------------------------------- Pie Char Brand Analysis ----------------------------------------------------
	 	    PieChart(data1,brandName,pietag);
	    //----------------------------------------------------------------------- Tweet keywords,mentions,hashtags -------------------------------------------------------------------
		    Tweets(data1,brand_tweetid,user_tweetid);
	    //--------------------------------------------------------------- Hash Tags -----------------------------------------------------------------------
		    Hash(data1,hashtag_id);
	    //--------------------------------------------------------------------------------- Good Tweets ---------------------------------------------------------------------
		    goodbad(data1,tweet_id,tweettry);




	},
	error: function(msg)
	{ 
	    //console.log(msg);
	    //console.log("beep!");
	}



    });



}


function histo1(pietag)
{
    //console.log("Please Work");
    google.load("visualization", "1", {packages:["corechart"]});
    var data = google.visualization.arrayToDataTable([
	    ['Task', 'Hours per Day'],
	    ['Work',     11],
	    ['Eat',      2],
	    ['Commute',  2],
	    ['Watch TV', 2],
	    ['Sleep',    7]
	    ]);

    var options = {
	title: 'My Daily Activities'
    };

    var chart = new google.visualization.PieChart(document.getElementById(pietag));
    chart.draw(data, options);
    //console.log(pietag);

}


function drawFacebookCharts(url,chartToDraw,brandName,queryType)
{
    var chartItems =  new Array();
    var myKeyVals = { 'brandname' : brandName, 'querytype':queryType}
    $.ajax({
	type:"POST",
	url:url,
	data:JSON.stringify(myKeyVals),
	dataType:"json",
	contentType:"application/json; charset=utf-8",
	success:function(data){ 
	    var chartTitle =  data.data.children[0].title
	var segments = data.data.children[0].countries;                             
    chartItems.push(['Country',chartTitle]);              
    for(i=0;i<segments.length;i++)
    {
	chartItems.push([segments[i].country,segments[i].count]);
    }           

    var dataCh = google.visualization.arrayToDataTable(chartItems);

    // //console.log(segments);
    // //console.log(data);
    // //console.log(chartItems);
    ////console.log(dataCh);
    ////console.log(data1);

    var geochart = new google.visualization.GeoChart(document.getElementById(chartToDraw));
    geochart.draw(dataCh, {width: '100%', height: '100%'});

	},
	error: function(msg)
	{ 
	    //console.log(msg);
	}
    });
}


function getPieChartByVisit(url,chartToDraw,xaxis,yaxis) {


    var chartItems =  new Array();
    $.ajax({
	type:"GET",
	url:url,
	dataType:"json",
	contentType:"application/json; charset=utf-8",
	success:function(data){  
	    var maleCnt = 0;
	    var femaleCnt = 0;
	    ////console.log(data.data.children);
	    var segments = data.data.children;              
	    chartItems.push([xaxis,yaxis]);              
	    for(i=0;i<segments.length;i++)
    {
	if(segments[i].sex=='male')
    {
	maleCnt = maleCnt + 1;
    }
	else
    {
	femaleCnt = femaleCnt + 1;
    }


    }   

    chartItems.push(["male",maleCnt]);
    chartItems.push(["female",femaleCnt]);
    ////console.log(segments.length);
    //console.log(chartItems);
    ////console.log(old);
    // var dataCh = google.visualization.arrayToDataTable(chartItems);
    //               var options = {
    //   title: yaxis,
    //   vAxis: {title: xaxis,  titleTextStyle: {color: 'red'}}
    // };

    // var chart = new google.visualization.BarChart(document.getElementById(chartToDraw));
    // chart.draw(dataCh, options);
    drawByVisitChart(chartItems,chartToDraw);


	},
	error: function(msg)
	{ 
	    //console.log(msg);
	}
    });
}


function drawByVisitChart(segments,chartToDraw) {
    var data = google.visualization.arrayToDataTable(segments);

    var options = {
	width: 'auto',
	height: '160',
	backgroundColor: 'transparent',
	colors: ['#ed6d49', '#74b749', '#0daed3', '#ffb400', '#f63131'],
	tooltip: {
	    textStyle: {
		color: '#666666',
		fontSize: 11
	    },
	    showColorCode: true
	},
	legend: {
	    position: 'left',
	    textStyle: {
		color: 'black',
		fontSize: 12
	    }
	},
	chartArea: {
	    left: 0,
	    top: 10,
	    width: "100%",
	    height: "100%"
	}
    };

    var chart = new google.visualization.PieChart(document.getElementById(chartToDraw));
    chart.draw(data, options);
}

//Geo Charts       
google.load('visualization', '1', {'packages': ['geochart']});  
google.load("visualization", "1", {'packages':["corechart"]});     

//Resize charts and graphs on window resize
$(document).ready(function () {
    $(window).resize(function(){
	drawByVisitChart(); 
	sparkline_graphs();         
    });
});
