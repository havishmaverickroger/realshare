
$(document).ready(function() {		

		urlString = "http://127.0.0.1:5000"
		//Segments
		getSegmentbyAge(urlString+"/segmentStats/age",'pieChartSegmentByAge');
		getSegmentbyAge(urlString+"/segmentStats/gender",'pieChartSegmentByGender');
		getSegmentbyAge(urlString+"/segmentStats/occupation",'pieChartSegmentByOccupation');
		getSegmentbyAge(urlString+"/segmentStats/income",'pieChartSegmentByIncome');
		getSegmentbyAge(urlString+"/segmentStats/race",'pieChartSegmentByRace');
		

		//UserData
		getSegmentbyAge(urlString+"/trafficStats/age",'pieChartUserDataAge');
		getSegmentbyAge(urlString+"/trafficStats/gender",'pieChartUserDataGender');
		getSegmentbyAge(urlString+"/trafficStats/occupation",'pieChartUserDataOccupation');
		getSegmentbyAge(urlString+"/trafficStats/income",'pieChartUserDataIncome');
		getSegmentbyAge(urlString+"/trafficStats/race",'pieChartUserDataRace');
		getSegmentbyAge(urlString+"/trafficStats/device",'pieChartUserDataDevice');		

    //Traffic
    getBarChartByVisit(urlString+"/trafficDataStats","chart_div","Frequency","visits");
    getBarChartByVisit(urlString+"/trafficDataDevice/device","device_div","Device","Quantity");
    getBarChartByVisit(urlString+"/trafficDataDevice/age","age_div","Age Group","Number");
    getBarChartByVisit(urlString+"/trafficDataDevice/occupation","occupation_div","Occupation","Count");
    getBarChartByVisit(urlString+"/trafficDataDevice/gender","gender_div","Gender","Count");
    getBarChartByVisit(urlString+"/trafficDataDevice/income","income_div","Income","Count");
    getBarChartByVisit(urlString+"/trafficDataDevice/race","race_div","Race","Count");


		sparkline_graphs();
		
    });

 function sparkline_graphs() {
        $(function () {
          $('#stock-graph').sparkline('html', {
            type: 'bar',
            barColor: '#0daed3',
            barWidth: 7,
            height: 38,
          });
        });
      }


      google.load("visualization", "1", {
        packages: ["corechart"]
      });



function getBarChartByVisit(url,chartToDraw,xaxis,yaxis) {
 

    var chartItems =  new Array();
       $.ajax({
       type:"GET",
       url:url,
       dataType:"json",
           contentType:"application/json; charset=utf-8",
           success:function(data){                
              var segments = data.data.children;              
              chartItems.push([xaxis,yaxis]);              
              for(i=0;i<segments.length;i++)
              {
                chartItems.push([segments[i].category,segments[i].count]);
              }   
            
              console.log(chartItems);
              //console.log(old);
              var dataCh = google.visualization.arrayToDataTable(chartItems);
                            var options = {
                title: yaxis,
                vAxis: {title: xaxis,  titleTextStyle: {color: 'red'}}
              };

              var chart = new google.visualization.BarChart(document.getElementById(chartToDraw));
              chart.draw(dataCh, options);


               },
                error: function(msg)
                { 
                console.log(msg);
                }
          });


   

      }



function getSegmentbyAge(url,chartToDraw)
{
	var segmentAgeitems =  new Array();
			 $.ajax({
	 		 type:"GET",
			 url:url,
			 dataType:"json",
	    		 contentType:"application/json; charset=utf-8",
	    		 success:function(data){  	    			 	
	    			 	var segments = data.data.children;	    			 	
	    			 	segmentAgeitems.push(['Age Group','Count']);	    			 	
	    			 	for(i=0;i<segments.length;i++)
	    			 	{
	    			 		segmentAgeitems.push([segments[i].category,segments[i].count]);
	    			 	}	    			 	
	    			 	drawByAgeChart(segmentAgeitems,chartToDraw);
	    			 	console.log();
	    				 },
	    	    		error: function(msg)
	    	    		{	
	    	    		console.log(msg);
	    	    		}
	    		});
}
    
      function drawByAgeChart(segments,chartToDraw) {
        var data = google.visualization.arrayToDataTable(segments);

        var options = {
          width: 'auto',
          height: '160',
          backgroundColor: 'transparent',
          colors: ['#ed6d49', '#74b749', '#0daed3', '#ffb400', '#f63131'],
          tooltip: {
            textStyle: {
              color: '#666666',
              fontSize: 11
            },
            showColorCode: true
          },
          legend: {
            position: 'left',
            textStyle: {
              color: 'black',
              fontSize: 12
            }
          },
          chartArea: {
            left: 0,
            top: 10,
            width: "100%",
            height: "100%"
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById(chartToDraw));
        chart.draw(data, options);
      }

      //Geo Charts
      google.load("visualization", "1", {'packages':["corechart"]});     
      
      //Resize charts and graphs on window resize
      $(document).ready(function () {
        $(window).resize(function(){
          drawByAgeChart(); 
          sparkline_graphs();         
        });
      });