import urllib2, json
import pymongo
from pymongo import MongoClient
from random import randrange
import random
import time
import datetime
from datetime import date
from dateutil.rrule import rrule, DAILY

class weather:
    def obtaining_data(self,city,startDate,endDate):
	city = city
	startDate = startDate
	endDate = endDate
	flag=0
	if(startDate==endDate):
		d = datetime.datetime.strptime(startDate , '%Y-%m-%d') + datetime.timedelta(days=1)
		endDate=d.strftime('%Y-%m-%d')
		flag=1
	s1=str(datetime.datetime.strptime(startDate, '%Y-%m-%d').strftime("%s"))
	e1=str(datetime.datetime.strptime(endDate, '%Y-%m-%d').strftime("%s"))

	if(flag==0):
		url="http://api.openweathermap.org/data/2.5/history/city?q="+city+"&type=day&start="+s1+"&end="+e1
	else:
		url="http://api.openweathermap.org/data/2.5/history/city?q="+city+"&type=hour&start="+s1+"&end="+e1
	response = urllib2.urlopen(url)
	j = json.load(response)
	return j


    def get_date(self,unidate):
		time2 = time.strftime("%D %H:%M", time.localtime(int(unidate)))
		return time2


    def temparatures(self,j):
		weather_dic={}

		for i in j:
			if(i!='list'):
				weather_dic[i]=j[i]
			else:
		 		weather_list=[]
		 		for k in j[i]:
		 			list_dic={}
					list_dic={'weather_data':k,'sales_count':randrange(100)}
					weather_list.append(list_dic)
				weather_dic[i]=weather_list
	
		results={}

		for i in weather_dic:
			if(i=="list"):
				for k in weather_dic[i]:
					date=self.get_date(k["weather_data"]["dt"]).split(" ")[0]
					temp=k["weather_data"]["main"]["temp"] - 273.15
					if(date not in results.keys()):
						results[str(date)]={'temp':0 , 'count':0}
					results[str(date)]['temp'] += temp
					results[str(date)]['count'] += 1
		return results
    
    def get_hour(self,date):
	    time=date.split(" ")[1].split(":")
	    if(int(time[1])!=0):
		    return int(time[0])+1
	    else:
	     	return int(time[0])

    def temparatures1(self,j):
		weather_dic={}
		for i in j:
			if(i!='list'):
				weather_dic[i]=j[i]
			else:
		 		weather_list=[]
		 		for k in j[i]:
		 			list_dic={}
					list_dic={'weather_data':k,'sales_count':randrange(100)}
					weather_list.append(list_dic)
				weather_dic[i]=weather_list
	
		results={}

		for i in weather_dic:
			if(i=="list"):
				for k in weather_dic[i]:
					date=self.get_date(k["weather_data"]["dt"])
					date=self.get_hour(date)
					temp=k["weather_data"]["main"]["temp"] - 273.15
					results[str(date)] = temp
		return results


    def dateset(self,date):
    	l=date.split("-")
    	l[2]=int(l[2])%2000
    	dt=str(l[0])+"-"+str(l[1])+"-"+str(l[2])
    	return dt

    def data_average(self,sDate,eDate,results):
	p1=sDate.split("-")
	p2=eDate.split("-")
	a=date(int(p1[0]),int(p1[1]),int(p1[2]))
	b=date(int(p2[0]),int(p2[1]),int(p2[2]))
	avg={}
    	print a,b	
	for dt in rrule(DAILY, dtstart=a, until=b):
	        k= dt.strftime("%Y-%m-%d")
		dt=self.dateset(k)
		if dt not in results.keys():
		    avg[k]=random.uniform(30.0,40.0)
		else:
			avg[k]=results[dt]['temp']/results[dt]['count']
	return avg


"""data=weather()
a1=data.obtaining_data("hyderabad,india","2014-06-04","2014-06-07")

results=data.temparatures(a1)
results=data.data_average("2014-06-04","2014-06-07",results)
print results"""






