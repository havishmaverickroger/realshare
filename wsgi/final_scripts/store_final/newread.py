import math
import boto
import os
import psycopg2
import threading
import pprint
class storeHandler:
	def macadd(self,filename):
		ma={}
		p1=open(filename,'r')
		line=p1.readlines()
		l1=len(line)
		for l in line:
			l=l.split(" ")
			p=l[1].split("\n")
			ma[str(l[0])]=p[0]
		return ma


	def final(self,file_lines):
		final_lines=[]
		for line in file_lines:
			line=line.replace("\n","")
			line=line.replace("\r","")
			if(len(line)!=0):
				final_lines.append(line)
		return final_lines

	def mapping(self,age_mapping):
		age=[]
		for line in age_mapping:
			line=line.replace("\n","")
			line=line.split(" ")
			age_range=line[0].split("-")
			age.append([int(age_range[0]),int(age_range[1]),line[1]])
		return age
	def get_age(self,age,age_mapping):
		for i in age_mapping:
			if(age>i[0] and age<i[1]):
				return i[2]
	def buildCheckString(self,id,date,hr,minute,gender,age,dwelltime):
	    	sqlString = "select count(*) from store where "
	    	sqlString = sqlString + "store.store_date = '" + date + "' and "
	    	sqlString = sqlString + "store.store_hour = " + hr + " and "
	    	sqlString = sqlString + "store.store_id = '" + id + "' and "
	    	sqlString = sqlString + "store.store_min = " + minute + " and "
	    	sqlString = sqlString + "store.store_gender = '" + gender + "' and "
	    	sqlString = sqlString + "store.store_age = '" + age + "' and "
	    	sqlString = sqlString + "store.store_dwelltime = '" + dwelltime  + "'"
	    	return sqlString

	def buildInsertString(self,keyid,id,date,hour,minute,gender,age,dwelltime):
	    	sqlString = "INSERT INTO store (storekey_id,store_id,store_date,store_hour,store_min,store_gender,store_age,store_dwelltime) values ( "
	    	sqlString = sqlString + keyid + "," 
	    	sqlString = sqlString + "'" +  id + "'," 
	    	sqlString = sqlString + "'" + date + "'," 
	    	sqlString = sqlString + hour + ","
	    	sqlString = sqlString + minute + ",'" + gender + "','" + age + "','" + dwelltime + "')"
		return sqlString

	def parsefile(self,filename1,ID):
		f1=open(filename1,'r')
		f4=open('age-div','r')
		mac=self.macadd("macaddress")
		lines=self.final(f1.readlines())
		store_Id=lines[1]
		if(store_Id not in mac.keys()):
			return
		p=lines[0].split("-")
		date=p[2]+"-"+p[1]+"-"+p[0]
		store=mac[str(store_Id)]
		
		 
		dic={}
		age_mapping = f4.readlines()
		age_mapping = self.mapping(age_mapping)
		
		for line in lines:
			if(line.find("Exited")!=-1):

				data = line.split(",")
				key_gender_age=data[2][0:data[2].find("[")].split("-")
				key=key_gender_age[0]
				dic[str(key)]={"store_id":store,"store_date":date}
				dic[str(key)]["store_gender"]=key_gender_age[1]
				dic[str(key)]["store_age"]=self.get_age(int(key_gender_age[2]),age_mapping)
				hour_min=data[0].split(":")
				dic[str(key)]["store_hour"]=int(hour_min[0])
				dic[str(key)]["store_min"]=int(hour_min[1])
				dwell_time=line.split("[")[1][:-1]
				dic[str(key)]["store_dwelltime"]=dwell_time
		

		for key in dic:
			dic[key]["store_keyid"]=str(ID)
			ID += 1
		return dic



	def fileLoader(self,dbname,user,host,password,directory):
		files=os.listdir(directory)
		sqlconn=psycopg2.connect("dbname='"+dbname+"' "+"user='"+user+"' "+"host='"+host+"' "+"password='"+password+"'")
		sqlcur=sqlconn.cursor()
		f=1 
		const=0
		for fi in files:
		    inputfile=directory+"/"+fi
		    sqlcur.execute("select count(*) from store;");
		    results = sqlcur.fetchone()
		    dic={}
		    if(inputfile.find(".swp")==-1):
		    	dic=self.parsefile(inputfile,results[0]+1)
		    else:
			    os.remove(inputfile)
		    for key in dic:
			sqlString=self.buildCheckString(dic[key]["store_id"],dic[key]["store_date"],str(dic[key]["store_hour"]),str(dic[key]["store_min"]),dic[key]["store_gender"],dic[key]["store_age"],dic[key]["store_dwelltime"])
			sqlcur.execute(sqlString)
			results = sqlcur.fetchone()
			if(results[0] == 0):
				sqlString1 = self.buildInsertString(dic[key]["store_keyid"],dic[key]["store_id"],dic[key]["store_date"],str(dic[key]["store_hour"]),str(dic[key]["store_min"]),dic[key]["store_gender"],dic[key]["store_age"],dic[key]["store_dwelltime"]) + ";"
				sqlcur.execute(sqlString1)
				print sqlString1
			else:
				    print ';---' 
		    sqlconn.commit()	
		    f=f+1
store=storeHandler()
store.fileLoader("realsharedb","postgres","localhost","mohanrao","store")







